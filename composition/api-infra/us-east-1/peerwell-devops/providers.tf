provider "aws" {
  version = "~> 2.11"
  region  = var.region
  profile = var.profile_name

  # assume_role {
  #   role_arn = "arn:aws:iam::${var.account_id}:role/${var.role_name}"
  # }
}

provider "aws" {
  version = "~> 2.11"
  region  = var.region
  alias   = "peerwell_old"
  profile = var.profile_name_peerwell_old
}

provider "local" {
  version = "~> 1.2"
}

provider "null" {
  version = "~> 2.1"
}

provider "template" {
  version = "~> 2.1"
}