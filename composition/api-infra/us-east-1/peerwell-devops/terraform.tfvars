## Provider ##
region                    = "us-east-1"
account_id                = "202536423779"
role_name                 = "Admin"
profile_name              = "peerwell-devops"
profile_name_peerwell_old = "peerwell-old"
# terraform_remote_state_bucket_name = "s3-ec1-eks-terraform-dev-backend-terraform-states"
# terraform_remote_state_key = "eks-cluster/eu-central-1/dev/terraform.tfstate"

## EKS ##
# worker_group_tags = {}

# # if set to true, AWS IAM Authenticator will use IAM role specified in "role_name" to authenticate to a cluster
# authenticate_using_role = false

# # if set to true, AWS IAM Authenticator will use AWS Profile name specified in profile_name to authenticate to a cluster instead of access key and secret access key
# authenticate_using_aws_profile = false

# # add other IAM users who can access a K8s cluster (by default, the IAM user who created a cluster is given access already)
# # map_users    = [
# #   {
# #     user_arn = "arn:aws:iam::024116961498:user/tester"
# #     username = "tester"
# #     group    = "system:masters"
# #   },
# # ]

# # how many groups of K8s worker nodes you want? Specify at least one group of worker node
# worker_groups = [
#     {
#       name                 = "worker-group-1"
#       instance_type        = "m3.large"
#       asg_max_size         = 2
#       asg_desired_capacity = 1
#     },
#     # {
#     #   name                 = "worker-group-2"
#     #   instance_type        = "m3.large"
#     #   asg_max_size         = 2
#     #   asg_desired_capacity = 1
#     # },
# ]

## KMS ##
enable_key_rotation = true

## VPC ##
cidr                  = "10.1.0.0/16" # don't overlap with peerwell-old account's VPC CIDR
azs                   = ["us-east-1a", "us-east-1b", "us-east-1c"]
public_subnets        = ["10.1.1.0/24", "10.1.2.0/24", "10.1.3.0/24"]
private_subnets       = ["10.1.101.0/24", "10.1.102.0/24", "10.1.103.0/24"]
database_subnets      = ["10.1.105.0/24", "10.1.106.0/24", "10.1.107.0/24"]
enable_dns_hostnames  = "true"
enable_dns_support    = "true"
enable_nat_gateway    = "true" # need internet connection for worker nodes in private subnets to be able to join the cluster 
single_nat_gateway    = "true"
peer_owner_id         = "164925596315" # peerwell-old AWS account
peer_vpc_id           = "vpc-3040aa54" # peerwell-old AWS account's VPC
peer_region           = "us-east-1"
accepter_cidr_block   = "10.0.0.0/16"    # peerwell-old AWS account's VPC CIDR
accepter_route_tables = ["rtb-7d0ed619"] # peerwell-old AWS account's private subnet's route table
peer_vpc_sg_id        = "sg-e3069f84"    # peerwell-old AWS account's SG for APIs and Mongo

## Public Security Group ##
public_ingress_with_cidr_blocks = [
]

## Metadata ##
env               = "prod"
app_name          = "peerwell-api-infra"
organization_name = "peerwell"

## DocDB cluster ##
apply_immediately               = false
availability_zones              = ["us-east-1a", "us-east-1b", "us-east-1c"]
backup_retention_period         = 30
enabled_cloudwatch_logs_exports = ["audit"]
engine_version                  = "3.6.0"
engine                          = "docdb"
final_snapshot_identifier       = "final-snapshot"
port                            = 27017
preferred_backup_window         = "02:00-05:00"
skip_final_snapshot             = false
# snapshot_identifier = 
storage_encrypted = true
ssm_parameter_docdb_master_username = {
  name  = "/docdb/master_username"
  value = "peerwell_dev" # changing this will force recreating DocDB
  type  = "String"
}

## DocDB instance ##
docdb_instance_count         = 1
auto_minor_version_upgrade   = true
docdb_instance_class               = "db.r4.large"
preferred_maintenance_window = "Sun:02:00-Sun:04:00"
# TODO: need to create config per each DocDB intance
promotion_tier = 0

## DocDB subnet group ##

## DocDB parameter group ##
family = "docdb3.6"
parameter = [
  {
    name  = "tls"
    value = "disabled"
  },
]

## DocDB password ##
random_password_length = 10
random_password_special = true
random_password_override_special = "%@"

## Database Security Group ##

###########################
##      Bastion Host     ##
###########################
bastion_app_name       = "bastion"
bastion_instance_count = 1
bastion_instance_type  = "t2.micro"
bastion_ami_id         = "ami-0b69ea66ff7391e80" # use static one instead of fetching it dynamically
bastion_private_ip     = ""
bastion_volume_size    = "40"
bastion_volume_type    = "gp2"
bastion_ingress_rules = [
  {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "Hisashi"
    cidr_blocks = "88.217.255.228/32"
  },
  {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "Hisashi"
    cidr_blocks = "62.216.206.234/32"
  },
  {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "Matthijn"
    cidr_blocks = "62.251.31.79/32"
  },
  {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "Jeff"
    cidr_blocks = "73.189.250.159/32"
  },
]

###########################
##     DB Controller Instance  ##
###########################
db_controller_instance_count = 1
db_controller_instance_type  = "t2.micro"
db_controller_ami_id         = "ami-0fa507ff42edff099" # containing mongo tools
db_controller_app_name       = "db_controller"
db_controller_private_ip     = ""
db_controller_volume_size    = "40"
db_controller_volume_type    = "gp2"
db_controller_ingress_rules  = []