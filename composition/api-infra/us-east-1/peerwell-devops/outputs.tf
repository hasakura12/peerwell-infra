## EKS ## 
# output "eks_cluster_name" {
#   description = "The name of the EKS cluster."
#   value       = module.eks.cluster_name
# }

# output "eks_cluster_id" {
#   description = "The id of the EKS cluster."
#   value       = module.eks.cluster_id
# }

# output "eks_cluster_arn" {
#   description = "The Amazon Resource Name (ARN) of the cluster."
#   value       = module.eks.cluster_arn
# }

# output "eks_cluster_certificate_authority_data" {
#   description = "Nested attribute containing certificate-authority-data for your cluster. This is the base64 encoded certificate data required to communicate with your cluster."
#   value       = module.eks.cluster_certificate_authority_data
# }

# output "eks_cluster_endpoint" {
#   description = "The endpoint for your EKS Kubernetes API."
#   value       = module.eks.cluster_endpoint
# }

# output "eks_cluster_version" {
#   description = "The Kubernetes server version for the EKS cluster."
#   value       = module.eks.cluster_version
# }

# output "eks_cluster_security_group_id" {
#   description = "Security group ID attached to the EKS cluster."
#   value       = module.eks.cluster_security_group_id
# }

# output "eks_config_map_aws_auth" {
#   description = "A kubernetes configuration to authenticate to this EKS cluster."
#   value       = module.eks.config_map_aws_auth
# }

# output "eks_cluster_iam_role_name" {
#   description = "IAM role name of the EKS cluster."
#   value       = module.eks.cluster_iam_role_name
# }

# output "eks_cluster_iam_role_arn" {
#   description = "IAM role ARN of the EKS cluster."
#   value       = module.eks.cluster_iam_role_arn
# }

# output "eks_cloudwatch_log_group_name" {
#   description = "Name of cloudwatch log group created"
#   value       = module.eks.cloudwatch_log_group_name
# }

# output "eks_kubeconfig" {
#   description = "kubectl config file contents for this EKS cluster."
#   value       = module.eks.kubeconfig
# }

# output "eks_kubeconfig_filename" {
#   description = "The filename of the generated kubectl config."
#   value       = module.eks.kubeconfig_filename
# }

# output "eks_workers_asg_arns" {
#   description = "IDs of the autoscaling groups containing workers."
#   value = module.eks.workers_asg_arns
# }

# output "eks_workers_asg_names" {
#   description = "Names of the autoscaling groups containing workers."
#   value = module.eks.workers_asg_names
# }

# output "eks_workers_user_data" {
#   description = "User data of worker groups"
#   value = module.eks.workers_user_data
# }

# output "eks_workers_default_ami_id" {
#   description = "ID of the default worker group AMI"
#   value       = module.eks.workers_default_ami_id
# }

# output "eks_workers_launch_template_ids" {
#   description = "IDs of the worker launch templates."
#   value       = module.eks.workers_launch_template_ids
# }

# output "eks_workers_launch_template_arns" {
#   description = "ARNs of the worker launch templates."
#   value       = module.eks.workers_launch_template_arns
# }

# output "eks_workers_launch_template_latest_versions" {
#   description = "Latest versions of the worker launch templates."
#   value       = module.eks.workers_launch_template_latest_versions
# }

# output "eks_worker_security_group_id" {
#   description = "Security group ID attached to the EKS workers."
#   value       = module.eks.worker_security_group_id
# }

# output "eks_worker_iam_instance_profile_arns" {
#   description = "default IAM instance profile ARN for EKS worker groups"
#   value       = module.eks.worker_iam_instance_profile_arns
# }

# output "eks_worker_iam_instance_profile_names" {
#   description = "default IAM instance profile name for EKS worker groups"
#   value       = module.eks.worker_iam_instance_profile_names
# }

# output "eks_worker_iam_role_name" {
#   description = "default IAM role name for EKS worker groups"
#   value = module.eks.worker_iam_role_name
# }

# output "eks_worker_iam_role_arn" {
#   description = "default IAM role ARN for EKS worker groups"
#   value = module.eks.worker_iam_role_arn
# }

## IAM ##
########################################
# K8s IAM Role
########################################
output "k8s_role_name" {
  description = "The name of the role."
  value       = module.iam.k8s_role_name
}

output "k8s_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = module.iam.k8s_role_arn
}

########################################
# View Only IAM Role
########################################
output "view_only_role_name" {
  description = "The name of the role."
  value       = module.iam.view_only_role_name
}

output "view_only_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = module.iam.view_only_role_arn
}

########################################
# Terraform Builder IAM Role
########################################
output "terraform_builder_role_name" {
  description = "The name of the role."
  value       = module.iam.terraform_builder_role_name
}

output "terraform_builder_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = module.iam.terraform_builder_role_arn
}

########################################
# VPC
########################################
output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = module.vpc.vpc_cidr_block
}

output "vpc_instance_tenancy" {
  description = "Tenancy of instances spin up within VPC"
  value       = module.vpc.vpc_instance_tenancy
}

output "vpc_enable_dns_support" {
  description = "Whether or not the VPC has DNS support"
  value       = module.vpc.vpc_enable_dns_support
}

output "vpc_enable_dns_hostnames" {
  description = "Whether or not the VPC has DNS hostname support"
  value       = module.vpc.vpc_enable_dns_hostnames
}

output "vpc_main_route_table_id" {
  description = "The ID of the main route table associated with this VPC"
  value       = module.vpc.vpc_main_route_table_id
}

output "vpc_secondary_cidr_blocks" {
  description = "List of secondary CIDR blocks of the VPC"
  value       = module.vpc.vpc_secondary_cidr_blocks
}

output "private_subnets" {
  description = "List of private subnets"
  value       = module.vpc.private_subnets
}

output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"
  value       = module.vpc.private_subnets_cidr_blocks
}

output "private_route_table_ids" {
  description = "List of IDs of private route tables"
  value       = module.vpc.private_route_table_ids
}

output "public_subnets" {
  description = "List of public subnets"
  value       = module.vpc.public_subnets
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = module.vpc.public_subnets_cidr_blocks
}

output "public_route_table_ids" {
  description = "List of IDs of public route tables"
  value       = module.vpc.public_route_table_ids
}

output "database_subnets" {
  description = "List of IDs of database subnets"
  value       = module.vpc.database_subnets
}

output "database_subnet_arns" {
  description = "List of ARNs of database subnets"
  value       = module.vpc.database_subnet_arns
}

output "database_subnets_cidr_blocks" {
  description = "List of cidr_blocks of database subnets"
  value       = module.vpc.database_subnets_cidr_blocks
}

output "database_route_table_ids" {
  description = "List of IDs of database route tables"
  value       = module.vpc.database_route_table_ids
}

output "database_subnet_group" {
  description = "ID of database subnet group"
  value       = module.vpc.database_subnet_group
}

output "nat_ids" {
  description = "List of allocation ID of Elastic IPs created { Gateway"
  value       = module.vpc.nat_ids
}

output "nat_public_ips" {
  description = "List of public Elastic IPs created { Gateway"
  value       = module.vpc.nat_public_ips
}

output "natgw_ids" {
  description = "List { IDs"
  value       = module.vpc.natgw_ids
}

output "igw_id" {
  description = "The ID of the Internet Gateway"
  value       = module.vpc.igw_id
}

output "public_network_acl_id" {
  description = "ID of the public network ACL"
  value       = module.vpc.public_network_acl_id
}

output "private_network_acl_id" {
  description = "ID of the private network ACL"
  value       = module.vpc.private_network_acl_id
}

output "database_network_acl_id" {
  description = "ID of the database network ACL"
  value       = module.vpc.database_network_acl_id
}

## Public Security Group ##
output "public_security_group_id" {
  value = module.vpc.public_security_group_id
}

output "public_security_group_vpc_id" {
  value = module.vpc.public_security_group_vpc_id
}

output "public_security_group_owner_id" {
  value = module.vpc.public_security_group_owner_id
}

output "public_security_group_name" {
  value = module.vpc.public_security_group_name
}

## Private Security Group ##
output "private_security_group_id" {
  value = module.vpc.private_security_group_id
}

output "private_security_group_vpc_id" {
  value = module.vpc.private_security_group_vpc_id
}

output "private_security_group_owner_id" {
  value = module.vpc.private_security_group_owner_id
}

output "private_security_group_name" {
  value = module.vpc.private_security_group_name
}

## Database Security Group ##
output "database_security_group_id" {
  value = module.vpc.database_security_group_id
}

output "database_security_group_vpc_id" {
  value = module.vpc.database_security_group_vpc_id
}

output "database_security_group_owner_id" {
  value = module.vpc.database_security_group_owner_id
}

output "database_security_group_name" {
  value = module.vpc.database_security_group_name
}

## VPC peering connection ##
output "vpc_peering_id" {
  description = "The ID of the VPC Peering Connection."
  value       = module.vpc.vpc_peering_id
}

output "vpc_peering_accept_status" {
  description = "The status of the VPC Peering Connection request."
  value       = module.vpc.vpc_peering_accept_status
}

########################################
# DocDB KMS
########################################
output "kms_key_docdb_arn" {
  description = "The Amazon Resource Name (ARN) of the AMI key."
  value       = module.docdb_kms_key.arn
}

output "kms_key_docdb_alias_arn" {
  description = "The Amazon Resource Name (ARN) of the AMI key alias."
  value       = module.docdb_kms_key.alias_arn
}

output "kms_key_docdb_id" {
  description = "The globally unique identifier for the AMI key."
  value       = module.docdb_kms_key.id
}

########################################
# DocDB Cluster
########################################
output "docdb_cluster_arn" {
  description = "Amazon Resource Name (ARN) of cluster"
  value       = module.docdb.cluster_arn
}
output "docdb_cluster_members" {
  description = "List of DocDB Instances that are a part of this cluster"
  value       = module.docdb.cluster_members
}
output "docdb_cluster_resource_id" {
  description = "The DocDB Cluster Resource ID"
  value       = module.docdb.cluster_resource_id
}
output "docdb_cluster_endpoint" {
  description = "The DNS address of the DocDB instance"
  value       = module.docdb.cluster_endpoint
}
output "docdb_hosted_zone_id" {
  description = "The Route53 Hosted Zone ID of the endpoint"
  value       = module.docdb.hosted_zone_id
}
output "docdb_cluster_id" {
  description = "The DocDB Cluster Identifier"
  value       = module.docdb.cluster_id
}
# output "maintenance_window" {
#   description = "The instance maintenance window"
#   value       = module.docdb.maintenance_window
# }
output "docdb_reader_endpoint" {
  description = "A read-only endpoint for the DocDB cluster, automatically load-balanced across replicas"
  value       = module.docdb.reader_endpoint
}

########################################
# DocDB Instance
########################################
output "docdb_instance_arn" {
  description = "Amazon Resource Name (ARN) of cluster instance"
  value       = module.docdb.instance_arn
}
output "docdb_instance_subnet_group_name" {
  description = "The DB subnet group to associate with this DB instance."
  value       = module.docdb.db_subnet_group_name
}
output "docdb_instance_dbi_resource_id" {
  description = "The region-unique, immutable identifier for the DB instance."
  value       = module.docdb.dbi_resource_id
}
output "docdb_instance_endpoint" {
  description = "The DNS address for this instance. May not be writable"
  value       = module.docdb.instance_endpoint
}
output "docdb_instance_engine_version" {
  description = "The database engine version"
  value       = module.docdb.engine_version
}
output "docdb_instance_kms_key_id" {
  description = "The ARN for the KMS encryption key if one is set to the cluster."
  value       = module.docdb.kms_key_id
}
output "docdb_instance_port" {
  description = "The database port"
  value       = module.docdb.port
}
output "docdb_instance_preferred_backup_window" {
  description = "The daily time range during which automated backups are created if automated backups are enabled."
  value       = module.docdb.preferred_backup_window
}
output "docdb_instance_storage_encrypted" {
  description = "Specifies whether the DB cluster is encrypted."
  value       = module.docdb.storage_encrypted
}
output "docdb_instance_writer" {
  description = "Boolean indicating if this instance is writable. False indicates this instance is a read replica."
  value       = module.docdb.writer
}

## DocDB subnet group ##
output "docdb_subnet_group_id" {
  description = "The docDB subnet group name."
  value       = module.docdb.subnet_group_id
}
output "docdb_subnet_group_arn" {
  description = "The ARN of the docDB subnet group."
  value       = module.docdb.subnet_group_arn
}

## DocDB parameter group ##
output "docdb_parameter_group_id" {
  description = "The documentDB cluster parameter group name."
  value       = module.docdb.parameter_group_id
}
output "docdb_parameter_group_arn" {
  description = "The ARN of the documentDB cluster parameter group."
  value       = module.docdb.parameter_group_arn
}

## SSM Parameter Store DocDB Master Username ##
output "ssm_parameter_docdb_master_username_arn" {
  description = "The ARN of the parameter."
  value       = module.docdb.ssm_parameter_docdb_master_username_arn
}
output "ssm_parameter_docdb_master_username_name" {
  description = "(Required) The name of the parameter."
  value       = module.docdb.ssm_parameter_docdb_master_username_name
}
output "ssm_parameter_docdb_master_username_description" {
  description = "(Required) The description of the parameter."
  value       = module.docdb.ssm_parameter_docdb_master_username_description
}
output "ssm_parameter_docdb_master_username_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.docdb.ssm_parameter_docdb_master_username_type
}
output "ssm_parameter_docdb_master_username_value" {
  description = "(Required) The value of the parameter."
  value       = module.docdb.ssm_parameter_docdb_master_username_value
}
output "ssm_parameter_docdb_master_username_version" {
  description = "The version of the parameter."
  value       = module.docdb.ssm_parameter_docdb_master_username_version
}

## SSM Parameter Store DocDB Master Password ##
output "ssm_parameter_docdb_master_password_arn" {
  description = "The ARN of the parameter."
  value       = module.docdb.ssm_parameter_docdb_master_password_arn
}
output "ssm_parameter_docdb_master_password_name" {
  description = "(Required) The name of the parameter."
  value       = module.docdb.ssm_parameter_docdb_master_password_name
}
output "ssm_parameter_docdb_master_password_description" {
  description = "(Required) The description of the parameter."
  value       = module.docdb.ssm_parameter_docdb_master_password_description
}
output "ssm_parameter_docdb_master_password_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.docdb.ssm_parameter_docdb_master_password_type
}
output "ssm_parameter_docdb_master_password_value" {
  description = "(Required) The value of the parameter."
  value       = module.docdb.ssm_parameter_docdb_master_password_value
}
output "ssm_parameter_docdb_master_password_version" {
  description = "The version of the parameter."
  value       = module.docdb.ssm_parameter_docdb_master_password_version
}

########################################
# Bastion Security Groups
########################################
output "bastion_security_group_id" {
  value = module.bastion.security_group_id
}

output "bastion_security_group_vpc_id" {
  value = module.bastion.security_group_vpc_id
}

output "bastion_security_group_owner_id" {
  value = module.bastion.security_group_owner_id
}

output "bastion_security_group_name" {
  value = module.bastion.security_group_name
}

########################################
# Bastion IAM Role
########################################
output "bastion_role_name" {
  description = "The name of the role."
  value       = module.bastion.role_name
}

output "bastion_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = module.bastion.role_arn
}

########################################
# Bastion IAM Instance Profile
########################################
output "bastion_instance_profile_id" {
  value = module.bastion.instance_profile_id
}

output "bastion_instance_profile_arn" {
  value = module.bastion.instance_profile_arn
}

output "bastion_create_date" {
  value = module.bastion.create_date
}

output "bastion_instance_profile_name" {
  value = module.bastion.instance_profile_name
}

output "bastion_instance_profile_role" {
  value = module.bastion.instance_profile_role
}

output "bastion_instance_profile_unique_id" {
  value = module.bastion.instance_profile_unique_id
}


########################################
# Bastion EC2
########################################
output "bastion_ec2_id" {
  value = module.bastion.ec2_availability_zone
}

output "bastion_ec2_placement_group" {
  value = module.bastion.ec2_placement_group
}

output "bastion_ec2_key_name" {
  value = module.bastion.ec2_key_name
}

output "bastion_ec2_password_data" {
  value       = module.bastion.ec2_password_data
  description = "Base-64 encoded encrypted password data for the instance. Useful for getting the administrator password for instances running Microsoft Windows. attribute is only exported if get_password_data is true. Note that encrypted value will be stored in the state file, as with all exported attributes. See GetPasswordData for more information."
}

output "bastion_ec2_public_dns" {
  value = module.bastion.ec2_public_dns
}

output "bastion_ec2_public_ip" {
  value = module.bastion.ec2_public_ip
}

output "bastion_ec2_primary_network_interface_id" {
  value = module.bastion.ec2_primary_network_interface_id
}

output "bastion_ec2_private_dns" {
  value = module.bastion.ec2_private_dns
}

output "bastion_ec2_private_ip" {
  value = module.bastion.ec2_private_ip
}

output "bastion_ec2_security_groups" {
  value = module.bastion.ec2_security_groups
}

output "bastion_ec2_vpc_security_group_ids" {
  value = module.bastion.ec2_vpc_security_group_ids
}

output "bastion_ec2_subnet_id" {
  value = module.bastion.ec2_subnet_id
}

########################################
# Bastion EC2 Key Pair
########################################
## AWS Key Pair ##
output "bastion_key_name" {
  description = "The key pair name."
  value       = module.bastion.key_name
}

output "bastion_fingerprint" {
  description = "The MD5 public key fingerprint as specified in section 4 of RFC 4716."
  value       = module.bastion.fingerprint
}

## TLS Private Key ##
output "bastion_algorithm" {
  description = "The algorithm that was selected for the key."
  value       = module.bastion.algorithm
}
output "bastion_public_key_pem" {
  description = "The public key data in PEM format."
  value       = module.bastion.public_key_pem
}
output "bastion_public_key_openssh" {
  description = "The public key data in OpenSSH authorized_keys format, if the selected private key format is compatible."
  value       = module.bastion.public_key_openssh
}
output "bastion_public_key_fingerprint_md5" {
  description = "The md5 hash of the public key data in OpenSSH MD5 hash format, e.g. aa:bb:cc:.... Only available if the selected private key format is compatible, as per the rules for"
  value       = module.bastion.public_key_fingerprint_md5
}

## SSM Parameter for public key ##
output "bastion_ssm_parameter_key_pair_public_key_arn" {
  description = "The ARN of the parameter."
  value       = module.bastion.ssm_parameter_key_pair_public_key_arn
}
output "bastion_ssm_parameter_key_pair_public_key_name" {
  description = "(Required) The name of the parameter."
  value       = module.bastion.ssm_parameter_key_pair_public_key_name
}
output "bastion_ssm_parameter_key_pair_public_key_description" {
  description = "(Required) The description of the parameter."
  value       = module.bastion.ssm_parameter_key_pair_public_key_description
}
output "bastion_ssm_parameter_key_pair_public_key_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.bastion.ssm_parameter_key_pair_public_key_type
}
output "bastion_ssm_parameter_key_pair_public_key_value" {
  description = "(Required) The value of the parameter."
  value       = module.bastion.ssm_parameter_key_pair_public_key_value
}
output "bastion_ssm_parameter_key_pair_public_key_version" {
  description = "The version of the parameter."
  value       = module.bastion.ssm_parameter_key_pair_public_key_version
}

## SSM Parameter for private key ##
output "bastion_ssm_parameter_key_pair_private_key_arn" {
  description = "The ARN of the parameter."
  value       = module.bastion.ssm_parameter_key_pair_private_key_arn
}
output "bastion_ssm_parameter_key_pair_private_key_name" {
  description = "(Required) The name of the parameter."
  value       = module.bastion.ssm_parameter_key_pair_private_key_name
}
output "bastion_ssm_parameter_key_pair_private_key_description" {
  description = "(Required) The description of the parameter."
  value       = module.bastion.ssm_parameter_key_pair_private_key_description
}
output "bastion_ssm_parameter_key_pair_private_key_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.bastion.ssm_parameter_key_pair_private_key_type
}
output "bastion_ssm_parameter_key_pair_private_key_version" {
  description = "The version of the parameter."
  value       = module.bastion.ssm_parameter_key_pair_private_key_version
}

## SSM Parameter for Bastion endpoint ##
output "bastion_ssm_parameter_bastion_endpoint_arn" {
  description = "The ARN of the parameter."
  value       = module.bastion.ssm_parameter_bastion_endpoint_arn
}
output "bastion_ssm_parameter_bastion_endpoint_name" {
  description = "(Required) The name of the parameter."
  value       = module.bastion.ssm_parameter_bastion_endpoint_name
}
output "bastion_ssm_parameter_bastion_endpoint_description" {
  description = "(Required) The description of the parameter."
  value       = module.bastion.ssm_parameter_bastion_endpoint_description
}
output "bastion_ssm_parameter_bastion_endpoint_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.bastion.ssm_parameter_bastion_endpoint_type
}
output "bastion_ssm_parameter_bastion_endpoint_version" {
  description = "The version of the parameter."
  value       = module.bastion.ssm_parameter_bastion_endpoint_version
}

########################################
# DB Controller Security Groups
########################################
output "db_controller_security_group_id" {
  value = module.db_controller_instance.security_group_id
}

output "db_controller_security_group_vpc_id" {
  value = module.db_controller_instance.security_group_vpc_id
}

output "db_controller_security_group_owner_id" {
  value = module.db_controller_instance.security_group_owner_id
}

output "db_controller_security_group_name" {
  value = module.db_controller_instance.security_group_name
}

########################################
# DB Controller IAM Role
########################################
output "db_controller_role_name" {
  description = "The name of the role."
  value       = "${module.db_controller_instance.role_name}"
}

output "db_controller_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = "${module.db_controller_instance.role_arn}"
}

########################################
# DB Controller IAM Instance Profile
########################################
output "db_controller_instance_profile_id" {
  value = "${module.db_controller_instance.instance_profile_id}"
}

output "db_controller_instance_profile_arn" {
  value = "${module.db_controller_instance.instance_profile_arn}"
}

output "db_controller_create_date" {
  value = "${module.db_controller_instance.create_date}"
}

output "db_controller_instance_profile_name" {
  value = "${module.db_controller_instance.instance_profile_name}"
}

output "db_controller_instance_profile_role" {
  value = "${module.db_controller_instance.instance_profile_role}"
}

output "db_controller_instance_profile_unique_id" {
  value = "${module.db_controller_instance.instance_profile_unique_id}"
}

########################################
# DB Controller EC2
########################################
output "db_controller_ec2_id" {
  value = "${module.db_controller_instance.ec2_id}"
}

output "db_controller_ec2_availability_zone" {
  value = "${module.db_controller_instance.ec2_availability_zone}"
}

output "db_controller_ec2_placement_group" {
  value = "${module.db_controller_instance.ec2_placement_group}"
}

output "db_controller_ec2_key_name" {
  value = "${module.db_controller_instance.ec2_key_name}"
}

output "db_controller_ec2_password_data" {
  value       = "${module.db_controller_instance.ec2_password_data}"
  description = "Base-64 encoded encrypted password data for the instance. Useful for getting the administrator password for instances running Microsoft Windows. attribute is only exported if get_password_data is true. Note that encrypted value will be stored in the state file, as with all exported attributes. See GetPasswordData for more information."
}

output "db_controller_ec2_public_dns" {
  value = "${module.db_controller_instance.ec2_public_dns}"
}

output "db_controller_ec2_public_ip" {
  value = "${module.db_controller_instance.ec2_public_ip}"
}

output "db_controller_ec2_primary_network_interface_id" {
  value = "${module.db_controller_instance.ec2_primary_network_interface_id}"
}

output "db_controller_ec2_private_dns" {
  value = "${module.db_controller_instance.ec2_private_dns}"
}

output "db_controller_ec2_private_ip" {
  value = "${module.db_controller_instance.ec2_private_ip}"
}

output "db_controller_ec2_security_groups" {
  value = "${module.db_controller_instance.ec2_security_groups}"
}

output "db_controller_ec2_vpc_security_group_ids" {
  value = "${module.db_controller_instance.ec2_vpc_security_group_ids}"
}

output "db_controller_ec2_subnet_id" {
  value = "${module.db_controller_instance.ec2_subnet_id}"
}

## SSM Parameter for DB Controller endpoint ##
output "db_controller_ssm_parameter_db_controller_endpoint_arn" {
  description = "The ARN of the parameter."
  value       = module.db_controller_instance.ssm_parameter_db_controller_endpoint_arn
}
output "db_controller_ssm_parameter_db_controller_endpoint_name" {
  description = "(Required) The name of the parameter."
  value       = module.db_controller_instance.ssm_parameter_db_controller_endpoint_name
}
output "db_controller_ssm_parameter_db_controller_endpoint_description" {
  description = "(Required) The description of the parameter."
  value       = module.db_controller_instance.ssm_parameter_db_controller_endpoint_description
}
output "db_controller_ssm_parameter_db_controller_endpoint_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.db_controller_instance.ssm_parameter_db_controller_endpoint_type
}
output "db_controller_ssm_parameter_db_controller_endpoint_version" {
  description = "The version of the parameter."
  value       = module.db_controller_instance.ssm_parameter_db_controller_endpoint_version
}