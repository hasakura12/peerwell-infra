terraform {
  backend "s3" {}
}

# module "eks" {
#   source = "../../../../infrastructure_module/eks"

#   cluster_name = var.cluster_name
#   vpc_id       = local.vpc_id
#   subnets      = local.subnets

#   worker_groups = var.worker_groups
#   worker_group_tags = local.tags

#   # add roles that can access K8s cluster
#   map_roles = local.map_roles
#   # add IAM users who can access K8s cluster
#   map_users = var.map_users

#   # specify AWS Profile if you want kubectl to use a named profile to authenticate instead of access key and secret access key
#   kubeconfig_aws_authenticator_env_variables = local.kubeconfig_aws_authenticator_env_variables

#   ## Common tag metadata ##
#   env      = var.env
#   app_name = var.app_name
#   tags     = local.tags
#   region   = var.region
# }

module "iam" {
  source = "../../../../infrastructure_module/iam"

  ## Common tag metadata ##
  env      = var.env
  app_name = var.app_name
  tags     = local.vpc_tags
  region   = var.region
}

module "docdb_kms_key" {
  source = "../../../../resource_module/identity/kms_key"

  name                    = local.ami_kms_key_name
  description             = local.ami_kms_key_description
  deletion_window_in_days = local.ami_kms_key_deletion_window_in_days
  tags                    = local.ami_kms_key_tags
  policy                  = data.aws_iam_policy_document.docdb_kms_key_policy.json
  enable_key_rotation     = var.enable_key_rotation
}

# DocDB master password
module "docdb_password" {
  source = "../../../../resource_module/identity/random_password"
  
  length = var.random_password_length
  special = var.random_password_special
  override_special = var.random_password_override_special
}

module "vpc" {
  source = "../../../../infrastructure_module/vpc"

  name                 = var.app_name
  cidr                 = var.cidr
  azs                  = var.azs
  private_subnets      = var.private_subnets
  public_subnets       = var.public_subnets
  database_subnets     = var.database_subnets
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support
  enable_nat_gateway   = var.enable_nat_gateway
  single_nat_gateway   = var.single_nat_gateway

  ## Public Security Group ##
  public_ingress_with_cidr_blocks = var.public_ingress_with_cidr_blocks

  ## Private Security Group ##
  peer_vpc_sg_id = var.peer_vpc_sg_id
  bastion_sg_id  = module.bastion.security_group_id

  ## Database security group ##
  databse_computed_ingress_with_source_security_group_id = module.db_controller_instance.security_group_id

  cluster_name = local.cluster_name

  ## VPC Peering ##
  # passing provider to grandchildren module: https://www.terraform.io/docs/configuration/providers.html
  providers = {
    aws.peerwell_old = "aws.peerwell_old"
  }

  peer_owner_id          = var.peer_owner_id
  peer_vpc_id            = var.peer_vpc_id
  peer_region            = var.peer_region
  requester_route_tables = module.vpc.private_route_table_ids
  accepter_cidr_block    = var.accepter_cidr_block
  accepter_route_tables  = var.accepter_route_tables
  requester_cidr_block   = var.cidr

  ## Common tag metadata ##
  env      = var.env
  app_name = var.app_name
  tags     = local.vpc_tags
  region   = var.region
}

module "docdb" {
  source = "../../../../infrastructure_module/docdb"

  ## DocDB cluster ##
  apply_immediately               = var.apply_immediately
  availability_zones              = var.availability_zones
  backup_retention_period         = var.backup_retention_period
  cluster_identifier              = local.cluster_identifier
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  engine_version                  = var.engine_version
  engine                          = var.engine
  final_snapshot_identifier       = var.final_snapshot_identifier
  kms_key_id                      = module.docdb_kms_key.arn
  port                            = var.port
  preferred_backup_window         = var.preferred_backup_window
  skip_final_snapshot             = var.skip_final_snapshot
  # snapshot_identifier = var.snapshot_identifier
  storage_encrypted      = var.storage_encrypted
  tags                   = local.docdb_cluster_tags
  vpc_security_group_ids = [module.vpc.database_security_group_id]

  ## DocDB instance ##
  docdb_instance_count         = var.docdb_instance_count
  auto_minor_version_upgrade   = var.auto_minor_version_upgrade
  identifier                   = local.identifier
  instance_class               = var.docdb_instance_class
  preferred_maintenance_window = var.preferred_maintenance_window
  promotion_tier               = var.promotion_tier
  docdb_instance_tags          = local.docdb_instance_tags

  ## DocDB subnet group ##
  subnet_group_name = local.subnet_group_name
  subnet_ids        = module.vpc.database_subnets
  subnet_group_tags = local.subnet_group_tags

  ## DocDB parameter group ##
  parameter_group_name = local.parameter_group_name
  family               = var.family
  parameter            = var.parameter
  parameter_group_tags = local.parameter_group_tags

  ## SSM Parameters ##
  ssm_parameter_docdb_master_username = var.ssm_parameter_docdb_master_username
  ssm_parameter_docdb_master_password = local.ssm_parameter_docdb_master_password

  ## Common tag metadata ##
  env      = var.env
  app_name = var.app_name
  region   = var.region
}

module "bastion" {
  source = "../../../../infrastructure_module/bastion"

  ## EC2 ##
  instance_count = var.bastion_instance_count
  # ami_id         = data.aws_ami.amazon_linux_2.id
  ami_id         = var.bastion_ami_id
  instance_type  = var.bastion_instance_type

  vpc_id      = module.vpc.vpc_id
  subnet_id   = module.vpc.public_subnets[0]
  private_ip  = var.bastion_private_ip
  volume_size = var.bastion_volume_size
  volume_type = var.bastion_volume_type

  ingress_with_cidr_blocks = var.bastion_ingress_rules

  ## Common tag metadata ##
  env      = var.env
  app_name = var.bastion_app_name
  region   = var.region

  tags            = local.tags
  region_tag      = var.region_tag
  environment_tag = var.environment_tag
}

module "db_controller_instance" {
  source = "../../../../infrastructure_module/db_controller_instance"

  ## EC2 ##
  instance_count = var.db_controller_instance_count
  ami_id         = var.db_controller_ami_id
  instance_type  = var.db_controller_instance_type

  vpc_id      = module.vpc.vpc_id
  subnet_id   = module.vpc.private_subnets[0]
  private_ip  = var.db_controller_private_ip
  volume_size = var.db_controller_volume_size
  volume_type = var.db_controller_volume_type

  ingress_with_cidr_blocks  = var.db_controller_ingress_rules
  source_security_group_ids = [module.bastion.security_group_id]

  # use key pair used by Bastion so we can ssh-forward
  key_name = module.bastion.key_name

  ## Common tag metadata ##
  env      = var.env
  app_name = var.db_controller_app_name
  region   = var.region

  tags            = local.tags
  region_tag      = var.region_tag
  environment_tag = var.environment_tag
}

module "governance" {
  source = "../../../../infrastructure_module/governance"

  # Cloudtrail
  organization_name = var.organization_name

  ## TODO: create Config rules

  ## Common tag metadata ##
  env    = var.env
  region = var.region
  tags   = local.tags
}