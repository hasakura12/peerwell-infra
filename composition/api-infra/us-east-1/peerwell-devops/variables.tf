## PROVIDER ##
variable "region" {}
variable "account_id" {}
variable "role_name" { default = "" }
variable "profile_name" {}
variable "profile_name_peerwell_old" {}
# variable "terraform_remote_state_bucket_name" { default = "" }
# variable "terraform_remote_state_key" { default = "" }

##  Common tag metadata ## 
variable "env" {}
variable "app_name" {}
variable "organization_name" {}
variable "region_tag" {
  type = "map"

  default = {
    "us-east-1"    = "ue1"
    "us-west-1"    = "uw1"
    "eu-west-1"    = "ew1"
    "eu-central-1" = "ec1"
  }
}
variable "environment_tag" {
  type = "map"

  default = {
    "prod"    = "prod"
    "qa"      = "qa"
    "staging" = "staging"
    "dev"     = "dev"
  }
}

## EKS ##
# variable "worker_groups" {
#   type        = any
#   default     = []
# }

# variable "worker_group_tags" {
#   type = map(string)
# }

# variable "authenticate_using_role" {
#   description = "if set to true, AWS IAM Authenticator will use IAM role specified in role_name to authenticate to a cluster"
# }

# variable "authenticate_using_aws_profile" {
#   description = "if set to true, AWS IAM Authenticator will use AWS Profile name specified in profile_name to authenticate to a cluster instead of access key and secret access key"
# }

# variable "map_roles" { 
#   type = list(map(string))
#   default = []
# }

# variable "map_users" { 
#   type = list(map(string))
#   default = []
# }

## KMS ##
variable "enable_key_rotation" {
  description = "(Optional) Specifies whether key rotation is enabled. Defaults to false."
}

## VPC ##
variable "cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  default     = "0.0.0.0/0"
}

variable "azs" {
  description = "Number of availability zones to use in the region"
  type        = list(string)
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  default     = []
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  default     = []
}

variable "database_subnets" {
  description = "A list of database subnets inside the VPC"
  default     = []
}

variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
  default     = true
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
  default     = true
}

variable "enable_nat_gateway" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks"
  default     = true
}

variable "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks"
  default     = true
}

## Private Security Group ##
variable "peer_vpc_sg_id" {}

## VPC Peering ##
variable "peer_owner_id" {}
variable "peer_vpc_id" {}
variable "peer_region" {}
variable "accepter_cidr_block" {}
variable "accepter_route_tables" {
  type = "list"
}

## DocDB Cluster ##
variable "apply_immediately" {
  description = "(Optional) Specifies whether any cluster modifications are applied immediately, or during the next maintenance window. Default is false."
}
variable "availability_zones" {
  description = "(Optional) A list of EC2 Availability Zones that instances in the DB cluster can be created in."
  type        = "list"
}
variable "backup_retention_period" {
  description = "(Optional) The days to retain backups for. Default 1"
}
variable "enabled_cloudwatch_logs_exports" {
  description = "(Optional) List of log types to export to cloudwatch. If omitted, no logs will be exported. The following log types are supported: audit."
}
variable "engine_version" {
  description = "(Optional) The database engine version. Updating this argument results in an outage."
}
variable "engine" {
  description = "(Optional) The name of the database engine to be used for this DB cluster. Defaults to docdb. Valid Values: docdb"
}
variable "final_snapshot_identifier" {
  description = "(Optional) The name of your final DB snapshot when this DB cluster is deleted. If omitted, no final snapshot will be made."
}
variable "port" {
  description = "(Optional) The port on which the DB accepts connections"
}
variable "preferred_backup_window" {
  description = "(Optional) The daily time range during which automated backups are created if automated backups are enabled using the BackupRetentionPeriod parameter.Time in UTC Default: A 30-minute window selected at random from an 8-hour block of time per region. e.g. 04:00-09:00"
}
variable "skip_final_snapshot" {
  description = "(Optional) Determines whether a final DB snapshot is created before the DB cluster is deleted. If true is specified, no DB snapshot is created. If false is specified, a DB snapshot is created before the DB cluster is deleted, using the value from final_snapshot_identifier. Default is false."
}
# variable "snapshot_identifier" {
#   description = "(Optional) Specifies whether or not to create this cluster from a snapshot. You can use either the name or ARN when specifying a DB cluster snapshot, or the ARN when specifying a DB snapshot."
# }
variable "storage_encrypted" {
  description = "(Optional) Specifies whether the DB cluster is encrypted. The default is false."
}
variable "ssm_parameter_docdb_master_username" {
  type        = map
  description = "A map of SSM parameter, requires the following keys: name, value"
}
variable "public_ingress_with_cidr_blocks" {
  type = list
}

## DocDB Instance ##
variable "auto_minor_version_upgrade" {
  description = "(Optional) Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window. Default true."
}
variable "docdb_instance_class" {
  description = "(Required) The instance class to use. For details on CPU and memory, see Scaling for DocDB Instances. DocDB currently supports the below instance classes. Please see AWS Documentation for complete details."
  default     = "db.r4.large"
  # other supported instance classes below:
  # db.r4.xlarge
  # db.r4.2xlarge
  # db.r4.4xlarge
  # db.r4.8xlarge
  # db.r4.16xlarge
}
variable "preferred_maintenance_window" {
  description = "(Optional) The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'. Eg: 'Mon:00:00-Mon:03:00'."
}
variable "promotion_tier" {
  description = "(Optional) Default 0. Failover Priority setting on instance level. The reader who has lower tier has higher priority to get promoter to writer."
}

## DocDB subnet group ##

## DocDB parameter group ##
variable "family" {
  description = "(Required, Forces new resource) The family of the documentDB cluster parameter group."
}
variable "parameter" {
  description = "(Optional) A list of documentDB parameters to apply."
  type        = list(map(string))
}

## DocDB password ##
variable "random_password_length" {
  description = "(Required) The length of the string desired"
}
variable "random_password_special" {
  description = "(Optional) (default true) Include special characters in random string. "
}
variable "random_password_override_special" {
  description = "(Optional) Supply your own list of special characters to use for string generation. This overrides the default character list in the special argument. The special argument must still be set to true for any overwritten characters to be used in generation."
}

#########################
##  Bastion Variables  ##
#########################

variable "bastion_app_name" {
  type = "string"
}
variable "bastion_instance_count" {}
variable "bastion_instance_type" {}
variable "bastion_ami_id" {}
variable "bastion_private_ip" {
  type = "string"
}
variable "bastion_volume_size" {
  type = "string"
}
variable "bastion_volume_type" {
  type = "string"
}
variable "bastion_ingress_rules" {
  type = "list"
}
variable "flow_logs_s3_bucket_arn" {
  default = ""
}

#########################
##  DB Controller Instance Variables  ##
#########################
variable "db_controller_instance_count" {}
variable "db_controller_instance_type" {}
variable "db_controller_ami_id" {}
variable "docdb_instance_count" {}
variable "db_controller_app_name" {
  type = "string"
}
variable "db_controller_private_ip" {
  type = "string"
}
variable "db_controller_volume_size" {
  type = "string"
}
variable "db_controller_volume_type" {
  type = "string"
}
variable "db_controller_ingress_rules" {
  type = "list"
}
variable "db_controller_flow_logs_s3_bucket_arn" {
  default = ""
}