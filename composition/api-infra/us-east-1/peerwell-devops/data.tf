locals {
  tags = {
    Environment = var.env
    Application = var.app_name
    Terraform   = true
  }

  ## DocDB KMS ##
  ami_kms_key_name                    = "alias/cmk-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-docdb"
  ami_kms_key_description             = "Kms key used for DocDB"
  ami_kms_key_deletion_window_in_days = "30"
  ami_kms_key_tags = merge(
    local.tags,
    map("Name", local.ami_kms_key_name)
  )

  ## VPC ##
  vpc_name = "vpc-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.app_name}"
  vpc_tags = merge(
    local.tags,
    map("VPC-Name", local.vpc_name)
  )

  ## DocDB ##
  cluster_identifier = "docdb-cluster-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.app_name}"
  docdb_cluster_tags = merge(
    local.tags,
    map("Name", local.cluster_identifier)
  )

  ## DocDB instance ##
  identifier = "docdb-instance-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.app_name}"
  docdb_instance_tags = merge(
    local.tags,
    map("Name", local.identifier)
  )

  ## DocDB subnet group ##
  subnet_group_name = "docdb-sbn-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.app_name}"
  subnet_group_tags = merge(
    local.tags,
    map("Name", local.subnet_group_name)
  )

  ## DocDB parameter group ##
  parameter_group_name = "docdb-param-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.app_name}"
  parameter_group_tags = merge(
    local.tags,
    map("Name", local.parameter_group_name)
  )

  ## SSM Param store ##
  ssm_parameter_docdb_master_password = {
    name  = "/docdb/master_password"
    value = module.docdb_password.result
    type  = "SecureString"
  }

  ## S3 remote state ##
  # key = "eks-cluster/eu-central-1/dev/terraform.tfstate"

  ## eks ##
  cluster_name = "eks-${var.app_name}"
  # vpc_id  = module.vpc.vpc_id
  # subnets = module.vpc.private_subnets
  # map_roles = var.authenticate_using_role == true ? concat(var.map_roles, [
  #   {
  #     role_arn = "arn:aws:iam::${var.account_id}:role/${var.role_name}"
  #     username = "role1"
  #     group    = "system:masters"
  #   },
  # ]) : var.map_roles

  # # specify AWS Profile if you want kubectl to use a named profile to authenticate instead of access key and secret access key
  # kubeconfig_aws_authenticator_env_variables = var.authenticate_using_aws_profile == true ? {
  #   AWS_PROFILE = var.profile_name
  # } : {}

  # tags = {
  #   Environment                                 = var.env
  #   Application                                 = var.app_name
  #   "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  # }
}

# data "terraform_remote_state" "base" {
#   backend = "s3"

#   config = {
#     bucket   = var.terraform_remote_state_bucket_name
#     key      = var.terraform_remote_state_key
#     region   = var.region
#     role_arn = "arn:aws:iam::${var.account_id}:role/${var.role_name}"
#   }
# }

data "aws_caller_identity" "this" {}

data "aws_iam_policy_document" "docdb_kms_key_policy" {
  statement {
    sid    = "Allow access for Key Administrators"
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:role/TerraformBuilder",
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:user/Hisashi",
      ]
    }

    actions = [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "Allow use of the key"
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root",
      ]
    }

    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "Allow attachment of persistent resources"
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root",
      ]
    }

    actions = [
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:RevokeGrant",
    ]

    resources = ["*"]

    condition {
      test     = "Bool"
      variable = "kms:GrantIsForAWSResource"
      values   = ["true"]
    }
  }
}

data "aws_ami" "amazon_linux_2" {
  # Finding Amazon Linux 2 AMI: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html
  # aws ec2 describe-images --owners amazon --filters 'Name=name,Values=amzn2-ami-hvm-2.0.????????-x86_64-gp2' 'Name=state,Values=available' --query 'reverse(sort_by(Images, &CreationDate))[:1].ImageId' --output text
  owners      = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}