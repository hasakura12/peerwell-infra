## S3 ##
output "s3_id" {
  description = "The name of the table"
  value       = module.backend.s3_id
}

output "s3_arn" {
  description = "The arn of the table"
  value       = module.backend.s3_arn
}

## DynamoDB ##
output "dynamodb_id" {
  description = "The name of the table"
  value       = module.backend.dynamodb_id
}

output "dynamodb_arn" {
  description = "The arn of the table"
  value       = module.backend.dynamodb_arn
}