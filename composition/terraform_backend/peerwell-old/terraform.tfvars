########################################
# Environment setting
########################################

region = "us-east-1"

role_name    = "Admin"
profile_name = "peerwell-old"

environment_name = "dev"

application_name = "peerwell-api-infra"

account_id = "164925596315"

app_name = "peerwell-api-infra"

acl = "private"

force_destroy = false

versioning_enabled = false

versioning_mfa_delete = false

sse_algorithm = "AES256"

block_public_policy = true

block_public_acls = true

ignore_public_acls = true

restrict_public_buckets = true

read_capacity = 5

write_capacity = 5

hash_key = "LockID"

sse_enabled = false

attribute_name = "LockID"

attribute_type = "S"
