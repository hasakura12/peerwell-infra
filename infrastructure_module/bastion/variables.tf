## EC2 ## 
variable "instance_count" {}
variable "instance_type" {}
variable "subnet_id" {
  type = "string"
}

variable "private_ip" {
  type = "string"
}

variable "ami_id" {
  type = "string"
}

variable "vpc_id" {
  type = "string"
}

## Metatada ##
variable "env" {
  type = "string"
}

variable "app_name" {
  type = "string"
}

variable "region" {
  type = "string"
}

## COMMON TAGS ## 
variable "region_tag" {
  type = "map"
}

variable "environment_tag" {
  type = "map"
}

variable "tags" {
  type = "map"
}

variable "volume_type" {
  type = "string"
}

variable "volume_size" {
  type = "string"
}

variable "ingress_with_cidr_blocks" {
  type = "list"
}
