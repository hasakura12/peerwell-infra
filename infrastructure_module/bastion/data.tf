locals {
  ## EC2 ##
  user_data                   = ""
  monitoring                  = true
  associate_public_ip_address = true
  ebs_optimized               = false
  volume_tags                 = {}
  root_block_device = [
    {
      volume_type = "${var.volume_type}"
      volume_size = "${var.volume_size}"
    },
  ]
  ebs_block_device                     = []
  ephemeral_block_device               = []
  source_dest_check                    = true
  disable_api_termination              = false
  instance_initiated_shutdown_behavior = ""
  tenancy                              = "default"
  ec2_name                             = "ec2-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.app_name}"
  ec2_tags = merge(
    var.tags,
    map("Name", local.ec2_name)
  )

  ## CloudWatch Alarm ##
  alarm_name          = "ec2-autorecover"
  namespace           = "AWS/EC2"
  evaluation_periods  = 2
  period              = 60
  alarm_description   = "This metric auto recovers EC2 instances"
  alarm_actions       = ["arn:aws:automate:${var.region}:ec2:recover"]
  statistic           = "Minimum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = 0
  metric_name         = "StatusCheckFailed_System"

  ## Bastion SG ##
  security_group_name                               = "scg-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.app_name}"
  security_group_description                        = "Security group for ${var.app_name}"
  ingress_with_cidr_blocks_local                    = []
  ingress_with_cidr_blocks                          = "${concat(var.ingress_with_cidr_blocks, local.ingress_with_cidr_blocks_local)}"
  computed_ingress_with_source_security_group_count = 0
  computed_ingress_with_source_security_group_id    = []
  security_group_tags = merge(
    var.tags,
    map("Name", local.security_group_name)
  )

  ## IAM Role ##
  iam_role_name                    = "${var.app_name}Ec2Role"
  iam_role_assume_role_policy_name = "${local.iam_role_name}AssumeRolePolicy"
  iam_role_policies = [
    {
      name   = "${local.iam_role_name}SsmGetPolicy"
      policy = data.aws_iam_policy_document.ssm_get_parameter.json
    },
  ]
  iam_role_policies_count = 1
  iam_role_tags = merge(
    var.tags,
    map("Name", local.iam_role_name)
  )

  ## Key Pair ##
  key_pair_name = "ec2-keypair-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.app_name}"
}

data "aws_iam_policy_document" "common_iam_role_assume_role_policy_document" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy" "policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

data "aws_iam_policy_document" "ssm_get_parameter" {
  statement {
    sid = "1"

    actions = [
      "ssm:Get*",
    ]

    resources = [
      "*",
    ]
  }
}