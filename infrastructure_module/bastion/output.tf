########################################
# Bastion Security Groups
########################################
output "security_group_id" {
  value = module.security_group.id
}

output "security_group_vpc_id" {
  value = module.security_group.vpc_id
}

output "security_group_owner_id" {
  value = module.security_group.owner_id
}

output "security_group_name" {
  value = module.security_group.name
}

########################################
# IAM Role
########################################
output "role_name" {
  description = "The name of the role."
  value       = "${module.iam_role.name}"
}

output "role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = "${module.iam_role.arn}"
}

########################################
# IAM Instance Profile
########################################
output "instance_profile_id" {
  value = "${module.instance_profile.id}"
}

output "instance_profile_arn" {
  value = "${module.instance_profile.arn}"
}

output "create_date" {
  value = "${module.instance_profile.create_date}"
}

output "instance_profile_name" {
  value = "${module.instance_profile.name}"
}

output "instance_profile_role" {
  value = "${module.instance_profile.role}"
}

output "instance_profile_unique_id" {
  value = "${module.instance_profile.unique_id}"
}

########################################
# Bastion EC2
########################################
output "ec2_id" {
  value = "${module.ec2.id}"
}

output "ec2_availability_zone" {
  value = "${module.ec2.availability_zone}"
}

output "ec2_placement_group" {
  value = "${module.ec2.placement_group}"
}

output "ec2_key_name" {
  value = "${module.ec2.key_name}"
}

output "ec2_password_data" {
  value       = "${module.ec2.password_data}"
  description = "Base-64 encoded encrypted password data for the instance. Useful for getting the administrator password for instances running Microsoft Windows. attribute is only exported if get_password_data is true. Note that encrypted value will be stored in the state file, as with all exported attributes. See GetPasswordData for more information."
}

output "ec2_public_dns" {
  value = "${module.ec2.public_dns}"
}

output "ec2_public_ip" {
  value = "${module.ec2.public_ip}"
}

output "ec2_primary_network_interface_id" {
  value = "${module.ec2.primary_network_interface_id}"
}

output "ec2_private_dns" {
  value = "${module.ec2.private_dns}"
}

output "ec2_private_ip" {
  value = "${module.ec2.private_ip}"
}

output "ec2_security_groups" {
  value = "${module.ec2.security_groups}"
}

output "ec2_vpc_security_group_ids" {
  value = "${module.ec2.vpc_security_group_ids}"
}

output "ec2_subnet_id" {
  value = "${module.ec2.subnet_id}"
}

########################################
# Bastion EC2 Key Pair
########################################
## AWS Key Pair ##
output "key_name" {
  description = "The key pair name."
  value       = module.key_pair.key_name
}

output "fingerprint" {
  description = "The MD5 public key fingerprint as specified in section 4 of RFC 4716."
  value       = module.key_pair.fingerprint
}

## TLS Private Key ##
output "algorithm" {
  description = "The algorithm that was selected for the key."
  value       = module.key_pair.algorithm
}
output "public_key_pem" {
  description = "The public key data in PEM format."
  value       = module.key_pair.public_key_pem
}
output "public_key_openssh" {
  description = "The public key data in OpenSSH authorized_keys format, if the selected private key format is compatible."
  value       = module.key_pair.public_key_openssh
}
output "public_key_fingerprint_md5" {
  description = "The md5 hash of the public key data in OpenSSH MD5 hash format, e.g. aa:bb:cc:.... Only available if the selected private key format is compatible, as per the rules for"
  value       = module.key_pair.public_key_fingerprint_md5
}

## SSM Parameter for public key ##
output "ssm_parameter_key_pair_public_key_arn" {
  description = "The ARN of the parameter."
  value       = module.ssm_parameter_key_pair_public_key.arn
}
output "ssm_parameter_key_pair_public_key_name" {
  description = "(Required) The name of the parameter."
  value       = module.ssm_parameter_key_pair_public_key.name
}
output "ssm_parameter_key_pair_public_key_description" {
  description = "(Required) The description of the parameter."
  value       = module.ssm_parameter_key_pair_public_key.description
}
output "ssm_parameter_key_pair_public_key_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.ssm_parameter_key_pair_public_key.type
}
output "ssm_parameter_key_pair_public_key_value" {
  description = "(Required) The value of the parameter."
  value       = module.ssm_parameter_key_pair_public_key.value
}
output "ssm_parameter_key_pair_public_key_version" {
  description = "The version of the parameter."
  value       = module.ssm_parameter_key_pair_public_key.version
}

## SSM Parameter for private key ##
output "ssm_parameter_key_pair_private_key_arn" {
  description = "The ARN of the parameter."
  value       = module.ssm_parameter_key_pair_private_key.arn
}
output "ssm_parameter_key_pair_private_key_name" {
  description = "(Required) The name of the parameter."
  value       = module.ssm_parameter_key_pair_private_key.name
}
output "ssm_parameter_key_pair_private_key_description" {
  description = "(Required) The description of the parameter."
  value       = module.ssm_parameter_key_pair_private_key.description
}
output "ssm_parameter_key_pair_private_key_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.ssm_parameter_key_pair_private_key.type
}
output "ssm_parameter_key_pair_private_key_version" {
  description = "The version of the parameter."
  value       = module.ssm_parameter_key_pair_private_key.version
}

## SSM Parameter for Bastion endpoint ##
output "ssm_parameter_bastion_endpoint_arn" {
  description = "The ARN of the parameter."
  value       = module.ssm_parameter_bastion_endpoint.arn
}
output "ssm_parameter_bastion_endpoint_name" {
  description = "(Required) The name of the parameter."
  value       = module.ssm_parameter_bastion_endpoint.name
}
output "ssm_parameter_bastion_endpoint_description" {
  description = "(Required) The description of the parameter."
  value       = module.ssm_parameter_bastion_endpoint.description
}
output "ssm_parameter_bastion_endpoint_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.ssm_parameter_bastion_endpoint.type
}
output "ssm_parameter_bastion_endpoint_version" {
  description = "The version of the parameter."
  value       = module.ssm_parameter_bastion_endpoint.version
}