locals {
  ## IAM Role ##
  k8s_iam_role_name                    = "k8sRole"
  k8s_iam_role_assume_role_policy_name = "${local.k8s_iam_role_name}AssumeRolePolicy"
  k8s_iam_role_policies                = []
  k8s_iam_role_policies_count          = 0
  k8s_iam_role_tags                    = "${merge(var.tags, map("Name", local.k8s_iam_role_name))}"

  ## view_only_iam_role ##
  view_only_iam_role_name                    = "ViewOnly"
  view_only_iam_role_assume_role_policy_name = "${local.view_only_iam_role_name}AssumeRolePolicy"
  view_only_iam_role_policies                = []
  view_only_iam_role_policies_count          = 0
  view_only_iam_role_tags                    = "${merge(var.tags, map("Name", local.view_only_iam_role_name))}"

  ## terraform_builder_iam_role ##
  terraform_builder_iam_role_name                    = "TerraformBuilder"
  terraform_builder_iam_role_assume_role_policy_name = "${local.terraform_builder_iam_role_name}AssumeRolePolicy"
  terraform_builder_iam_role_policies                = []
  terraform_builder_iam_role_policies_count          = 0
  terraform_builder_iam_role_tags                    = "${merge(var.tags, map("Name", local.terraform_builder_iam_role_name))}"
}

data "aws_caller_identity" "this" {}

# Account assume role policy
data "aws_iam_policy_document" "common_iam_role_assume_role_policy_document" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root",
      ]
    }
  }
}

data "aws_iam_policy_document" "k8s_iam_role_assume_role_policy_document" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy" "eks_cluster_policy" {
  arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

data "aws_iam_policy" "eks_service_policy" {
  arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
}

data "aws_iam_policy" "view_only_role_policy" {
  arn = "arn:aws:iam::aws:policy/job-function/ViewOnlyAccess"
}
data "aws_iam_policy" "terraform_builder_policy" {
  arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

data "aws_iam_policy" "code_commit_read_only_policy" {
  arn = "arn:aws:iam::aws:policy/AWSCodeCommitReadOnly"
}