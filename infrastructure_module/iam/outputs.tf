########################################
# K8s IAM Role
########################################
output "k8s_role_name" {
  description = "The name of the role."
  value       = "${module.k8s_iam_role.name}"
}

output "k8s_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = "${module.k8s_iam_role.arn}"
}

########################################
# View Only IAM Role
########################################
output "view_only_role_name" {
  description = "The name of the role."
  value       = "${module.view_only_iam_role.name}"
}

output "view_only_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = "${module.view_only_iam_role.arn}"
}

########################################
# Terraform Builder IAM Role
########################################
output "terraform_builder_role_name" {
  description = "The name of the role."
  value       = "${module.terraform_builder_iam_role.name}"
}

output "terraform_builder_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = "${module.terraform_builder_iam_role.arn}"
}
