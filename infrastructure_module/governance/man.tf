########################################
## Cloudtrail Access Log S3 Bucket
########################################
module "cloudtrail_access_log_s3_bucket" {
  source = "../../resource_module/storage/s3"

  bucket        = local.cloudtrail_access_log_s3_bucket_name
  acl           = local.cloudtrail_access_log_s3_acl
  policy        = data.aws_iam_policy_document.cloudtrail_access_log_s3_bucket_policy.json
  tags          = local.cloudtrail_access_log_s3_bucket_tags
  force_destroy = local.cloudtrail_access_log_s3_force_destroy
  region        = var.region

  website                              = local.cloudtrail_access_log_s3_website
  cors_rule                            = local.cloudtrail_access_log_s3_cors_rule
  versioning                           = local.cloudtrail_access_log_s3_versioning
  logging                              = local.cloudtrail_access_log_s3_logging
  lifecycle_rule                       = local.cloudtrail_access_log_s3_lifecycle_rule
  replication_configuration            = local.cloudtrail_access_log_s3_replication_configuration
  server_side_encryption_configuration = local.cloudtrail_access_log_s3_server_side_encryption_configuration
  object_lock_configuration            = local.cloudtrail_access_log_s3_object_lock_configuration

  ## s3 bucket public access block ##
  block_public_policy     = local.cloudtrail_access_log_s3_block_public_policy
  block_public_acls       = local.cloudtrail_access_log_s3_block_public_acls
  ignore_public_acls      = local.cloudtrail_access_log_s3_ignore_public_acls
  restrict_public_buckets = local.cloudtrail_access_log_s3_restrict_public_buckets
}

########################################
## Cloudtrail S3 Bucket
########################################
module "cloudtrail_s3_bucket" {
  source = "../../resource_module/storage/s3"

  bucket        = local.cloudtrail_s3_bucket_name
  acl           = local.cloudtrail_s3_acl
  policy        = data.aws_iam_policy_document.cloudtrail_s3_bucket_policy.json
  tags          = local.cloudtrail_s3_bucket_tags
  force_destroy = local.cloudtrail_s3_force_destroy
  region        = var.region

  website                              = local.cloudtrail_s3_website
  cors_rule                            = local.cloudtrail_s3_cors_rule
  versioning                           = local.cloudtrail_s3_versioning
  logging                              = local.cloudtrail_s3_logging
  lifecycle_rule                       = local.cloudtrail_s3_lifecycle_rule
  replication_configuration            = local.cloudtrail_s3_replication_configuration
  server_side_encryption_configuration = local.cloudtrail_s3_server_side_encryption_configuration
  object_lock_configuration            = local.cloudtrail_s3_object_lock_configuration

  ## s3 bucket public access block ##
  block_public_policy     = local.cloudtrail_s3_block_public_policy
  block_public_acls       = local.cloudtrail_s3_block_public_acls
  ignore_public_acls      = local.cloudtrail_s3_ignore_public_acls
  restrict_public_buckets = local.cloudtrail_s3_restrict_public_buckets
}

########################################
## Cloudtrail
########################################
module "cloudtrail" {
  source = "../../resource_module/governance/cloudtrail"

  name                          = local.cloudtrail_name
  s3_bucket_name                = module.cloudtrail_s3_bucket.id
  s3_key_prefix                 = local.cloudtrail_s3_key_prefix
  include_global_service_events = local.cloudtrail_include_global_service_events
  enable_log_file_validation    = local.cloudtrail_enable_log_file_validation
  is_multi_region_trail         = local.cloudtrail_is_multi_region_trail
  is_organization_trail         = local.cloudtrail_is_organization_trail
}

########################################
## KMS
########################################
module "s3_kms_key" {
  source = "../../resource_module/identity/kms_key"

  name                    = local.ami_kms_key_name
  description             = local.ami_kms_key_description
  deletion_window_in_days = local.ami_kms_key_deletion_window_in_days
  tags                    = local.ami_kms_key_tags
  policy                  = data.aws_iam_policy_document.cloudtrail_s3_kms_key_policy.json
  enable_key_rotation     = true
}