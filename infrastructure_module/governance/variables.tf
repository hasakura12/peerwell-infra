variable "organization_name" {
  description = "The name of the customer organization."
  type        = "string"
}

variable "region" {
  description = "The AWS region this bucket should reside in."
  type        = "string"
}

variable "region_tag" {
  type = "map"

  default = {
    "us-east-1"    = "ue1"
    "us-west-1"    = "uw1"
    "eu-west-1"    = "ew1"
    "eu-central-1" = "ec1"
  }
}

variable "env" {}

variable "environment_tag" {
  type = "map"

  default = {
    "prod"     = "p"
    "pre_prod" = "pp"
    "qa"       = "q"
    "staging"  = "s"
    "dev"      = "d"
  }
}

variable "tags" {
  description = "A mapping of tags to assign to all resources."
  type        = "map"
}