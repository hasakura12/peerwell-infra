########################################
# Cloudtrail Access Log S3 Bucket 
########################################

output "cloudtrail_access_log_s3_bucket_id" {
  description = "The name of the bucket."
  value       = module.cloudtrail_access_log_s3_bucket.id
}

output "cloudtrail_access_log_s3_bucket_arn" {
  description = "The ARN of the bucket."
  value       = module.cloudtrail_access_log_s3_bucket.arn
}

output "cloudtrail_access_log_s3_bucket_region" {
  description = "The AWS region this bucket resides in."
  value       = module.cloudtrail_access_log_s3_bucket.region
}

########################################
# Cloudtrail S3 Bucket
########################################

output "cloudtrail_s3_bucket_id" {
  description = "The name of the bucket."
  value       = module.cloudtrail_s3_bucket.id
}

output "cloudtrail_s3_bucket_arn" {
  description = "The ARN of the bucket."
  value       = module.cloudtrail_s3_bucket.arn
}

output "cloudtrail_s3_bucket_region" {
  description = "The AWS region this bucket resides in."
  value       = module.cloudtrail_s3_bucket.region
}

########################################
# Governance
########################################
# Cloudtrail
output "cloudtrail_arn" {
  description = "The Amazon Resource Name of the trail."
  value       = module.cloudtrail.arn
}

output "cloudtrail_id" {
  description = "The name of the trail."
  value       = module.cloudtrail.id
}

output "cloudtrail_home_region" {
  description = "The region in which the trail was created."
  value       = module.cloudtrail.home_region
}