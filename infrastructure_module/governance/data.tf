locals {
  ########################################
  ## Cloudtrail Access Log S3 Bucket
  ########################################
  cloudtrail_access_log_s3_bucket_name   = "s3-${var.region_tag[var.region]}-${lower(var.organization_name)}-${var.env}-cloudtrail-access-log"
  cloudtrail_access_log_s3_acl           = "log-delivery-write"
  cloudtrail_access_log_s3_bucket_tags   = merge(var.tags, map("Name", local.cloudtrail_s3_bucket_name))
  cloudtrail_access_log_s3_force_destroy = false
  cloudtrail_access_log_s3_website       = {}
  cloudtrail_access_log_s3_cors_rule     = {}
  cloudtrail_access_log_s3_versioning = {
    enabled = true
    # can't seem to be done from TF
    # mfa_delete = true
  }
  cloudtrail_access_log_s3_logging = {}
  cloudtrail_access_log_s3_lifecycle_rule = [
    {
      id      = "log_current_version"
      enabled = true
      prefix  = "log/"

      tags = {
        rule      = "log"
        autoclean = "false"
      }

      transition = [
        {
          days          = 30
          storage_class = "ONEZONE_IA"
        },
        {
          days          = 60
          storage_class = "GLACIER"
        }
      ]
    },
    {
      id                                     = "log_non_current_version"
      enabled                                = true
      prefix                                 = "log/"
      abort_incomplete_multipart_upload_days = 7

      noncurrent_version_transition = [
        {
          days          = 30
          storage_class = "ONEZONE_IA"
        },
        {
          days          = 60
          storage_class = "GLACIER"
        },
      ]
    },
  ]
  cloudtrail_access_log_s3_replication_configuration = {}
  cloudtrail_access_log_s3_server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = module.s3_kms_key.arn
      }
    }
  }
  cloudtrail_access_log_s3_object_lock_configuration = {
    object_lock_enabled = "Enabled"
    rule = {
      default_retention = {
        mode  = "GOVERNANCE"
        years = 1
      }
    }
  }

  ## aws_s3 bucket public access block ## 
  cloudtrail_access_log_s3_block_public_policy     = true
  cloudtrail_access_log_s3_block_public_acls       = true
  cloudtrail_access_log_s3_ignore_public_acls      = true
  cloudtrail_access_log_s3_restrict_public_buckets = true

  ########################################
  ## Cloudtrail S3 Bucket
  ########################################
  cloudtrail_s3_bucket_name   = "s3-${var.region_tag[var.region]}-${lower(var.organization_name)}-${var.env}-cloudtrail"
  cloudtrail_s3_acl           = "private"
  cloudtrail_s3_bucket_tags   = merge(var.tags, map("Name", local.cloudtrail_s3_bucket_name))
  cloudtrail_s3_force_destroy = false

  cloudtrail_s3_website   = {}
  cloudtrail_s3_cors_rule = {}
  cloudtrail_s3_versioning = {
    enabled = true
    # can't seem to be done from TF
    # mfa_delete = true
  }
  cloudtrail_s3_logging = {
    target_bucket = module.cloudtrail_access_log_s3_bucket.id
    target_prefix = "log/"
  }
  cloudtrail_s3_lifecycle_rule = [
    {
      id      = "log_current_version"
      enabled = true
      prefix  = "log/"

      tags = {
        rule      = "log"
        autoclean = "false"
      }

      transition = [
        {
          days          = 30
          storage_class = "ONEZONE_IA"
        },
        {
          days          = 60
          storage_class = "GLACIER"
        }
      ]
    },
    {
      id                                     = "log_non_current_version"
      enabled                                = true
      prefix                                 = "log/"
      abort_incomplete_multipart_upload_days = 7

      noncurrent_version_transition = [
        {
          days          = 30
          storage_class = "ONEZONE_IA"
        },
        {
          days          = 60
          storage_class = "GLACIER"
        },
      ]
    },
  ]
  cloudtrail_s3_replication_configuration = {}
  cloudtrail_s3_server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = module.s3_kms_key.arn
      }
    }
  }
  cloudtrail_s3_object_lock_configuration = {
    object_lock_enabled = "Enabled"
    rule = {
      default_retention = {
        mode  = "GOVERNANCE"
        years = 1
      }
    }
  }

  ## aws_s3_bucket_public_access_block ## 
  cloudtrail_s3_block_public_policy     = true
  cloudtrail_s3_block_public_acls       = true
  cloudtrail_s3_ignore_public_acls      = true
  cloudtrail_s3_restrict_public_buckets = true

  ########################################
  ## Cloudtrail
  ########################################
  cloudtrail_name                          = "ct-global-${lower(var.organization_name)}-${var.env}-trail"
  cloudtrail_s3_key_prefix                 = "log/"
  cloudtrail_include_global_service_events = true
  cloudtrail_enable_log_file_validation    = true
  cloudtrail_is_multi_region_trail         = true
  cloudtrail_is_organization_trail         = false

  ########################################
  ## KMS
  ########################################
  ami_kms_key_name                    = "alias/cmk-${var.region_tag[var.region]}-${var.env}-s3-cloudtrail"
  ami_kms_key_description             = "Kms key used for Cloudtrail trails in S3"
  ami_kms_key_deletion_window_in_days = "30"
  ami_kms_key_tags = merge(
    var.tags,
    map("Name", local.ami_kms_key_name)
  )
}

data "aws_caller_identity" "this" {}

# Cloudtrail Access Log S3 Bucket Policy
data "aws_iam_policy_document" "cloudtrail_access_log_s3_bucket_policy" {
  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
    ]

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root",
      ]
    }

    resources = [
      "arn:aws:s3:::${local.cloudtrail_access_log_s3_bucket_name}/*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:PutObject",
    ]

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root",
      ]
    }

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

    resources = [
      "arn:aws:s3:::${local.cloudtrail_access_log_s3_bucket_name}/*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:PutObject",
    ]

    principals {
      type = "Service"

      identifiers = [
        "config.amazonaws.com",
        "cloudtrail.amazonaws.com",
        "delivery.logs.amazonaws.com",
      ]
    }

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

    resources = [
      "arn:aws:s3:::${local.cloudtrail_access_log_s3_bucket_name}/*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:GetBucketAcl",
      "s3:GetBucketPolicy",
    ]

    principals {
      type = "Service"

      identifiers = [
        "config.amazonaws.com",
        "cloudtrail.amazonaws.com",
        "delivery.logs.amazonaws.com",
      ]
    }

    resources = [
      "arn:aws:s3:::${local.cloudtrail_access_log_s3_bucket_name}",
    ]
  }
}

# Cloudtrail S3 Bucket Policy
data "aws_iam_policy_document" "cloudtrail_s3_bucket_policy" {
  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
    ]

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root",
      ]
    }

    resources = [
      "arn:aws:s3:::${local.cloudtrail_s3_bucket_name}/*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:PutObject",
    ]

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root",
      ]
    }

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

    resources = [
      "arn:aws:s3:::${local.cloudtrail_s3_bucket_name}/*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:PutObject",
    ]

    principals {
      type = "Service"

      identifiers = [
        "config.amazonaws.com",
        "cloudtrail.amazonaws.com",
        "delivery.logs.amazonaws.com",
      ]
    }

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

    resources = [
      "arn:aws:s3:::${local.cloudtrail_s3_bucket_name}/*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:GetBucketAcl",
      "s3:GetBucketPolicy",
    ]

    principals {
      type = "Service"

      identifiers = [
        "config.amazonaws.com",
        "cloudtrail.amazonaws.com",
        "delivery.logs.amazonaws.com",
      ]
    }

    resources = [
      "arn:aws:s3:::${local.cloudtrail_s3_bucket_name}",
    ]
  }
}

data "aws_iam_policy_document" "cloudtrail_s3_kms_key_policy" {
  statement {
    sid    = "Allow access for Key Administrators"
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:role/TerraformBuilder",
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:user/Hisashi",
      ]
    }

    actions = [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "Allow use of the key"
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root",
      ]
    }

    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "Allow attachment of persistent resources"
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root",
      ]
    }

    actions = [
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:RevokeGrant",
    ]

    resources = ["*"]

    condition {
      test     = "Bool"
      variable = "kms:GrantIsForAWSResource"
      values   = ["true"]
    }
  }
}