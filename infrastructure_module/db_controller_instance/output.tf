########################################
# DB Controller Security Groups
########################################
output "security_group_id" {
  value = module.security_group.id
}

output "security_group_vpc_id" {
  value = module.security_group.vpc_id
}

output "security_group_owner_id" {
  value = module.security_group.owner_id
}

output "security_group_name" {
  value = module.security_group.name
}

########################################
# IAM Role
########################################
output "role_name" {
  description = "The name of the role."
  value       = "${module.iam_role.name}"
}

output "role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = "${module.iam_role.arn}"
}

########################################
# IAM Instance Profile
########################################
output "instance_profile_id" {
  value = "${module.instance_profile.id}"
}

output "instance_profile_arn" {
  value = "${module.instance_profile.arn}"
}

output "create_date" {
  value = "${module.instance_profile.create_date}"
}

output "instance_profile_name" {
  value = "${module.instance_profile.name}"
}

output "instance_profile_role" {
  value = "${module.instance_profile.role}"
}

output "instance_profile_unique_id" {
  value = "${module.instance_profile.unique_id}"
}

########################################
# DB Controller EC2
########################################
output "ec2_id" {
  value = "${module.ec2.id}"
}

output "ec2_availability_zone" {
  value = "${module.ec2.availability_zone}"
}

output "ec2_placement_group" {
  value = "${module.ec2.placement_group}"
}

output "ec2_key_name" {
  value = "${module.ec2.key_name}"
}

output "ec2_password_data" {
  value       = "${module.ec2.password_data}"
  description = "Base-64 encoded encrypted password data for the instance. Useful for getting the administrator password for instances running Microsoft Windows. attribute is only exported if get_password_data is true. Note that encrypted value will be stored in the state file, as with all exported attributes. See GetPasswordData for more information."
}

output "ec2_public_dns" {
  value = "${module.ec2.public_dns}"
}

output "ec2_public_ip" {
  value = "${module.ec2.public_ip}"
}

output "ec2_primary_network_interface_id" {
  value = "${module.ec2.primary_network_interface_id}"
}

output "ec2_private_dns" {
  value = "${module.ec2.private_dns}"
}

output "ec2_private_ip" {
  value = "${module.ec2.private_ip}"
}

output "ec2_security_groups" {
  value = "${module.ec2.security_groups}"
}

output "ec2_vpc_security_group_ids" {
  value = "${module.ec2.vpc_security_group_ids}"
}

output "ec2_subnet_id" {
  value = "${module.ec2.subnet_id}"
}

## SSM Parameter for DB Controller endpoint ##
output "ssm_parameter_db_controller_endpoint_arn" {
  description = "The ARN of the parameter."
  value       = module.ssm_parameter_db_controller_endpoint.arn
}
output "ssm_parameter_db_controller_endpoint_name" {
  description = "(Required) The name of the parameter."
  value       = module.ssm_parameter_db_controller_endpoint.name
}
output "ssm_parameter_db_controller_endpoint_description" {
  description = "(Required) The description of the parameter."
  value       = module.ssm_parameter_db_controller_endpoint.description
}
output "ssm_parameter_db_controller_endpoint_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.ssm_parameter_db_controller_endpoint.type
}
output "ssm_parameter_db_controller_endpoint_version" {
  description = "The version of the parameter."
  value       = module.ssm_parameter_db_controller_endpoint.version
}