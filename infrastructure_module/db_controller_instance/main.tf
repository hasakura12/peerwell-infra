module "security_group" {
  source = "../../resource_module/compute/security_group"

  name        = local.security_group_name
  description = local.security_group_description
  vpc_id      = var.vpc_id

  ingress_with_cidr_blocks                                 = local.ingress_with_cidr_blocks
  computed_ingress_with_source_security_group_id           = local.computed_ingress_with_source_security_group_id
  number_of_computed_ingress_with_source_security_group_id = local.computed_ingress_with_source_security_group_count

  egress_rules = ["all-all"]

  tags = local.security_group_tags
}

module "iam_role" {
  source                      = "../../resource_module/identity/iam/role"
  name                        = local.iam_role_name
  tags                        = local.iam_role_tags
  assume_role_policy_name     = local.iam_role_assume_role_policy_name
  assume_role_policy_document = data.aws_iam_policy_document.common_iam_role_assume_role_policy_document.json
  policies                    = local.iam_role_policies
  policies_count              = local.iam_role_policies_count
  policies_arns               = [data.aws_iam_policy.policy.arn]
}

module "instance_profile" {
  source = "../../resource_module/identity/iam/instance_profile"
  name   = "${module.iam_role.name}InstanceProfile"
  role   = module.iam_role.name
}

module "ec2" {
  source = "../../resource_module/compute/ec2"

  name           = local.ec2_name
  instance_count = var.instance_count
  ami            = var.ami_id
  instance_type  = var.instance_type
  user_data      = local.user_data
  subnet_id      = var.subnet_id
  key_name       = var.key_name
  monitoring     = local.monitoring

  vpc_security_group_ids = [
    module.security_group.id
  ]

  iam_instance_profile                 = module.instance_profile.name
  associate_public_ip_address          = local.associate_public_ip_address
  private_ip                           = var.private_ip
  ebs_optimized                        = local.ebs_optimized
  volume_tags                          = local.volume_tags
  root_block_device                    = local.root_block_device
  ebs_block_device                     = local.ebs_block_device
  ephemeral_block_device               = local.ephemeral_block_device
  source_dest_check                    = local.source_dest_check
  disable_api_termination              = local.disable_api_termination
  instance_initiated_shutdown_behavior = local.instance_initiated_shutdown_behavior
  tenancy                              = local.tenancy
  tags                                 = local.ec2_tags

  ## CloudWatch Alarm ##
  alarm_name          = local.alarm_name
  namespace           = local.namespace
  evaluation_periods  = local.evaluation_periods
  period              = local.period
  alarm_description   = local.alarm_description
  alarm_actions       = local.alarm_actions
  statistic           = local.statistic
  comparison_operator = local.comparison_operator
  threshold           = local.threshold
  metric_name         = local.metric_name
}

module "ssm_parameter_db_controller_endpoint" {
  source = "../../resource_module/compute/ssm"

  ssm_parameter = {
    name  = "/${var.app_name}/endpoint"
    value = module.ec2.public_dns[0]
    type  = "String"
  }
}