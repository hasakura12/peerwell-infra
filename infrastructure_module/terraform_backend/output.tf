## S3 ##
output "s3_id" {
  description = "The name of the table"
  value       = "${module.s3.id}"
}

output "s3_arn" {
  description = "The arn of the table"
  value       = "${module.s3.arn}"
}

## DynamoDB ##

output "dynamodb_id" {
  description = "The name of the table"
  value       = "${module.dynamodb.id}"
}

output "dynamodb_arn" {
  description = "The arn of the table"
  value       = "${module.dynamodb.arn}"
}