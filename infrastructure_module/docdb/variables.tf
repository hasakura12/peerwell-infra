##  Common tag metadata ## 
variable "region" {}
variable "env" {}
variable "app_name" {}
variable "region_tag" {
  type = "map"

  default = {
    "us-east-1"    = "ue1"
    "us-west-1"    = "uw1"
    "eu-west-1"    = "ew1"
    "eu-central-1" = "ec1"
  }
}
variable "environment_tag" {
  type = "map"

  default = {
    "prod"    = "prod"
    "qa"      = "qa"
    "staging" = "staging"
    "dev"     = "dev"
  }
}

## DocDB Cluster ##
variable "apply_immediately" {
  description = "(Optional) Specifies whether any cluster modifications are applied immediately, or during the next maintenance window. Default is false."
}
variable "availability_zones" {
  description = "(Optional) A list of EC2 Availability Zones that instances in the DB cluster can be created in."
  type        = "list"
}
variable "backup_retention_period" {
  description = "(Optional) The days to retain backups for. Default 1"
}
variable "cluster_identifier" {
  description = "(Optional, Forces new resources) The cluster identifier. If omitted, Terraform will assign a random, unique identifier."
}
variable "enabled_cloudwatch_logs_exports" {
  description = "(Optional) List of log types to export to cloudwatch. If omitted, no logs will be exported. The following log types are supported: audit."
}
variable "engine_version" {
  description = "(Optional) The database engine version. Updating this argument results in an outage."
}
variable "engine" {
  description = "(Optional) The name of the database engine to be used for this DB cluster. Defaults to docdb. Valid Values: docdb"
}
variable "final_snapshot_identifier" {
  description = "(Optional) The name of your final DB snapshot when this DB cluster is deleted. If omitted, no final snapshot will be made."
}
variable "kms_key_id" {
  description = "(Optional) The ARN for the KMS encryption key. When specifying kms_key_id, storage_encrypted needs to be set to true."
}
variable "port" {
  description = "(Optional) The port on which the DB accepts connections"
}
variable "preferred_backup_window" {
  description = "(Optional) The daily time range during which automated backups are created if automated backups are enabled using the BackupRetentionPeriod parameter.Time in UTC Default: A 30-minute window selected at random from an 8-hour block of time per region. e.g. 04:00-09:00"
}
variable "skip_final_snapshot" {
  description = "(Optional) Determines whether a final DB snapshot is created before the DB cluster is deleted. If true is specified, no DB snapshot is created. If false is specified, a DB snapshot is created before the DB cluster is deleted, using the value from final_snapshot_identifier. Default is false."
}
# variable "snapshot_identifier" {
#   description = "(Optional) Specifies whether or not to create this cluster from a snapshot. You can use either the name or ARN when specifying a DB cluster snapshot, or the ARN when specifying a DB snapshot."
# }
variable "storage_encrypted" {
  description = "(Optional) Specifies whether the DB cluster is encrypted. The default is false."
}
variable "tags" {
  description = "(Optional) A mapping of tags to assign to the DB cluster."
}
variable "vpc_security_group_ids" {
  description = "(Optional) List of VPC security groups to associate with the Cluster"
  type        = list
}

## DocDB Instance ##
variable "docdb_instance_count" {}
variable "auto_minor_version_upgrade" {
  description = "(Optional) Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window. Default true."
}
variable "identifier" {
  description = "(Optional, Forces new resource) The indentifier for the DocDB instance, if omitted, Terraform will assign a random, unique identifier."
}
variable "instance_class" {
  description = "(Required) The instance class to use. For details on CPU and memory, see Scaling for DocDB Instances. DocDB currently supports the below instance classes. Please see AWS Documentation for complete details."
  default     = "db.r4.large"
  # other supported instance classes below:
  # db.r4.xlarge
  # db.r4.2xlarge
  # db.r4.4xlarge
  # db.r4.8xlarge
  # db.r4.16xlarge
}
variable "preferred_maintenance_window" {
  description = "(Optional) The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'. Eg: 'Mon:00:00-Mon:03:00'."
}
variable "promotion_tier" {
  description = "(Optional) Default 0. Failover Priority setting on instance level. The reader who has lower tier has higher priority to get promoter to writer."
}
variable "docdb_instance_tags" {}

## DocDB subnet group ##
variable "subnet_group_name" {
  description = "(Optional, Forces new resource) The name of the docDB subnet group. If omitted, Terraform will assign a random, unique name."
}
variable "subnet_ids" {
  description = "(Required) A list of VPC subnet IDs."
}
variable "subnet_group_tags" {
  description = "(Optional) A mapping of tags to assign to the resource."
}

## DocDB parameter group ##
variable "parameter_group_name" {
  description = "(Optional, Forces new resource) The name of the documentDB cluster parameter group. If omitted, Terraform will assign a random, unique name."
}
variable "family" {
  description = "(Required, Forces new resource) The family of the documentDB cluster parameter group."
}
variable "parameter" {
  description = "(Optional) A list of documentDB parameters to apply."
  type        = list(map(string))
}
variable "parameter_group_tags" {
  description = "(Optional) A mapping of tags to assign to the resource."
}

## SSM Parameters ##
variable "ssm_parameter_docdb_master_username" {
  type        = map
  description = "A map of SSM parameter, requires the following keys: name, value"
}
variable "ssm_parameter_docdb_master_password" {
  type        = map
  description = "A map of SSM parameter, requires the following keys: name, value"
}