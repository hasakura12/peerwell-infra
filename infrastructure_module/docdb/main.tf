module "docdb" {
  source = "../../resource_module/database/docdb"

  ## DocDB cluster ##
  apply_immediately               = var.apply_immediately
  availability_zones              = var.availability_zones
  backup_retention_period         = var.backup_retention_period
  cluster_identifier              = var.cluster_identifier
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  engine_version                  = var.engine_version
  engine                          = var.engine
  final_snapshot_identifier       = "${var.final_snapshot_identifier}-${substr(timestamp(), 0, 10)}"
  kms_key_id                      = var.kms_key_id
  master_username                 = module.ssm_parameter_docdb_master_username.value
  master_password                 = module.ssm_parameter_docdb_master_password.value
  port                            = var.port
  preferred_backup_window         = var.preferred_backup_window
  skip_final_snapshot             = var.skip_final_snapshot
  # snapshot_identifier             = var.snapshot_identifier
  storage_encrypted      = var.storage_encrypted
  tags                   = var.tags
  vpc_security_group_ids = var.vpc_security_group_ids

  ## DocDB instance ##
  docdb_instance_count         = var.docdb_instance_count
  auto_minor_version_upgrade   = var.auto_minor_version_upgrade
  identifier                   = var.identifier
  instance_class               = var.instance_class
  preferred_maintenance_window = var.preferred_maintenance_window
  promotion_tier               = var.promotion_tier
  docdb_instance_tags          = var.docdb_instance_tags

  ## DocDB subnet group ##
  subnet_group_name = local.subnet_group_name
  subnet_ids        = var.subnet_ids
  subnet_group_tags = var.subnet_group_tags

  ## DocDB parameter group ##
  parameter_group_name = local.parameter_group_name
  family               = var.family
  parameter            = var.parameter
  parameter_group_tags = var.parameter_group_tags
}

module "ssm_parameter_docdb_master_username" {
  source = "../../resource_module/compute/ssm"

  ssm_parameter = var.ssm_parameter_docdb_master_username
}

module "ssm_parameter_docdb_master_password" {
  source = "../../resource_module/compute/ssm"

  ssm_parameter = var.ssm_parameter_docdb_master_password
}

module "ssm_parameter_docdb_cluster_endpoint" {
  source = "../../resource_module/compute/ssm"

  ssm_parameter = {
    name  = "/docdb/cluster_endpoint"
    value = module.docdb.cluster_endpoint
    type  = "String"
  }
}

module "ssm_parameter_docdb_reader_endpoint" {
  source = "../../resource_module/compute/ssm"

  ssm_parameter = {
    name  = "/docdb/reader_endpoint"
    value = module.docdb.reader_endpoint
    type  = "String"
  }
}