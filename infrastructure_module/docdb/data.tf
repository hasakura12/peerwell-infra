locals {
  ## DocDB subnet group ##
  subnet_group_name = "docdb-sbngrp-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.app_name}"

  ## DocDB parameter group ##
  parameter_group_name = "docdb-param-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.app_name}"
}