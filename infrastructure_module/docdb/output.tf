## DocDB Cluster ##
output "cluster_arn" {
  description = "Amazon Resource Name (ARN) of cluster"
  value       = module.docdb.cluster_arn
}
output "cluster_members" {
  description = "List of DocDB Instances that are a part of this cluster"
  value       = module.docdb.cluster_members
}
output "cluster_resource_id" {
  description = "The DocDB Cluster Resource ID"
  value       = module.docdb.cluster_resource_id
}
output "cluster_endpoint" {
  description = "The DNS address of the DocDB instance"
  value       = module.docdb.cluster_endpoint
}
output "hosted_zone_id" {
  description = "The Route53 Hosted Zone ID of the endpoint"
  value       = module.docdb.hosted_zone_id
}
output "cluster_id" {
  description = "The DocDB Cluster Identifier"
  value       = module.docdb.cluster_id
}
# output "maintenance_window" {
#   description = "The instance maintenance window"
#   value       = module.docdb.maintenance_window
# }
output "reader_endpoint" {
  description = "A read-only endpoint for the DocDB cluster, automatically load-balanced across replicas"
  value       = module.docdb.reader_endpoint
}

## DocDB Instance ##
output "instance_arn" {
  description = "Amazon Resource Name (ARN) of cluster instance"
  value       = module.docdb.instance_arn
}
output "db_subnet_group_name" {
  description = "The DB subnet group to associate with this DB instance."
  value       = module.docdb.db_subnet_group_name
}
output "dbi_resource_id" {
  description = "The region-unique, immutable identifier for the DB instance."
  value       = module.docdb.dbi_resource_id
}
output "instance_endpoint" {
  description = "The DNS address for this instance. May not be writable"
  value       = module.docdb.instance_endpoint
}
output "engine_version" {
  description = "The database engine version"
  value       = module.docdb.engine_version
}
output "kms_key_id" {
  description = "The ARN for the KMS encryption key if one is set to the cluster."
  value       = module.docdb.kms_key_id
}
output "port" {
  description = "The database port"
  value       = module.docdb.port
}
output "preferred_backup_window" {
  description = "The daily time range during which automated backups are created if automated backups are enabled."
  value       = module.docdb.preferred_backup_window
}
output "storage_encrypted" {
  description = "Specifies whether the DB cluster is encrypted."
  value       = module.docdb.storage_encrypted
}
output "writer" {
  description = "Boolean indicating if this instance is writable. False indicates this instance is a read replica."
  value       = module.docdb.writer
}

## DocDB subnet group ##
output "subnet_group_id" {
  description = "The docDB subnet group name."
  value       = module.docdb.subnet_group_id
}
output "subnet_group_arn" {
  description = "The ARN of the docDB subnet group."
  value       = module.docdb.subnet_group_arn
}

## DocDB parameter group ##
output "parameter_group_id" {
  description = "The documentDB cluster parameter group name."
  value       = module.docdb.parameter_group_id
}
output "parameter_group_arn" {
  description = "The ARN of the documentDB cluster parameter group."
  value       = module.docdb.parameter_group_arn
}

## SSM Parameter Store DocDB Master Username ##
output "ssm_parameter_docdb_master_username_arn" {
  description = "The ARN of the parameter."
  value       = module.ssm_parameter_docdb_master_username.arn
}
output "ssm_parameter_docdb_master_username_name" {
  description = "(Required) The name of the parameter."
  value       = module.ssm_parameter_docdb_master_username.name
}
output "ssm_parameter_docdb_master_username_description" {
  description = "(Required) The description of the parameter."
  value       = module.ssm_parameter_docdb_master_username.description
}
output "ssm_parameter_docdb_master_username_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.ssm_parameter_docdb_master_username.type
}
output "ssm_parameter_docdb_master_username_value" {
  description = "(Required) The value of the parameter."
  value       = module.ssm_parameter_docdb_master_username.value
}
output "ssm_parameter_docdb_master_username_version" {
  description = "The version of the parameter."
  value       = module.ssm_parameter_docdb_master_username.version
}

## SSM Parameter Store DocDB Master Password ##
output "ssm_parameter_docdb_master_password_arn" {
  description = "The ARN of the parameter."
  value       = module.ssm_parameter_docdb_master_password.arn
}
output "ssm_parameter_docdb_master_password_name" {
  description = "(Required) The name of the parameter."
  value       = module.ssm_parameter_docdb_master_password.name
}
output "ssm_parameter_docdb_master_password_description" {
  description = "(Required) The description of the parameter."
  value       = module.ssm_parameter_docdb_master_password.description
}
output "ssm_parameter_docdb_master_password_type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = module.ssm_parameter_docdb_master_password.type
}
output "ssm_parameter_docdb_master_password_value" {
  description = "(Required) The value of the parameter."
  value       = module.ssm_parameter_docdb_master_password.value
}
output "ssm_parameter_docdb_master_password_version" {
  description = "The version of the parameter."
  value       = module.ssm_parameter_docdb_master_password.version
}