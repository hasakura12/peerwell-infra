locals {
  ## VPC ##  

  // need to tag public subnet with "shared" so K8s can find right subnets to create ELBs
  // https://github.com/kubernetes/kubernetes/issues/29298
  internet_gw_prefix = "igw"
  nat_gw_prefix      = "ngw"
  route_table_prefix = "rtb"
  subnet_prefix      = "sbn"
  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "Tier"                                      = "public"
  }

  # need tag for internal-elb to be able to create ELB
  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"           = "true"
    "Tier"                                      = "private"
  }

  database_subnet_tags = {
    "Tier" = "database"
  }

  ## Public SG ##
  public_security_group_name        = "scg-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-public"
  public_security_group_description = "Security group for public subnets"
  public_security_group_tags = merge(
    var.tags,
    map("Name", local.public_security_group_name), map("Tier", "public"),
    map("Tier", "public")
  )

  ## Private SG ##
  private_security_group_name        = "scg-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-private"
  private_security_group_description = "Security group for private subnets"
  private_security_group_tags = merge(
    var.tags,
    map("Name", local.private_security_group_name),
    map("Tier", "private")
  )

  ## DB SG ##
  db_security_group_name        = "scg-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-database"
  db_security_group_description = "Security group for database subnets"
  db_security_group_tags = merge(
    var.tags,
    map("Name", local.db_security_group_name),
    map("Tier", "database")
  )

  ## VPC Peering ##
  vpc_peering_tags = merge(
    var.tags,
    map("Name", "vpc-peering-${var.region_tag[var.region]}-${var.environment_tag[var.env]}-${var.name}"),
  )
}