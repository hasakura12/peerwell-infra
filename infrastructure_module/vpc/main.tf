module "vpc" {
  source = "../../resource_module/network/vpc"

  name = var.name
  cidr = var.cidr

  azs              = var.azs
  private_subnets  = var.private_subnets
  public_subnets   = var.public_subnets
  database_subnets = var.database_subnets

  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  enable_nat_gateway = var.enable_nat_gateway
  single_nat_gateway = var.single_nat_gateway

  tags                 = var.tags
  internet_gw_prefix   = local.internet_gw_prefix
  nat_gw_prefix        = local.nat_gw_prefix
  route_table_prefix   = local.route_table_prefix
  subnet_prefix        = local.subnet_prefix
  public_subnet_tags   = local.public_subnet_tags
  private_subnet_tags  = local.private_subnet_tags
  database_subnet_tags = local.database_subnet_tags
}

module "public_security_group" {
  source = "../../resource_module/compute/security_group"

  name        = local.public_security_group_name
  description = local.public_security_group_description
  vpc_id      = module.vpc.vpc_id

  ingress_rules = ["http-80-tcp","https-443-tcp"]
  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_with_cidr_blocks = var.public_ingress_with_cidr_blocks

  egress_rules = ["all-all"]
  tags         = local.public_security_group_tags
}

module "private_security_group" {
  source = "../../resource_module/compute/security_group"

  name        = local.private_security_group_name
  description = local.private_security_group_description
  vpc_id      = module.vpc.vpc_id

  computed_ingress_with_source_security_group_id = [
    {
      rule                     = "http-80-tcp"
      source_security_group_id = module.public_security_group.id
      description              = "Port 80 from public SG rule"
    },
    {
      rule                     = "https-443-tcp"
      source_security_group_id = module.public_security_group.id
      description              = "Port 443 from public SG rule"
    },
    {
      rule                     = "ssh-tcp"
      source_security_group_id = var.bastion_sg_id
      description              = "SSH from bastion SG rule"
    },
  ]

  ingress_with_self = [
    {
      rule = "all-all"
      description = "Self"
    },
  ]

  number_of_computed_ingress_with_source_security_group_id = 3
  egress_rules                                             = ["all-all"]
  tags                                                     = local.private_security_group_tags
}

module "database_security_group" {
  source = "../../resource_module/compute/security_group"

  name        = local.db_security_group_name
  description = local.db_security_group_description
  vpc_id      = module.vpc.vpc_id

  computed_ingress_with_source_security_group_id = [
    {
      rule                     = "mongodb-27017-tcp"
      source_security_group_id = module.private_security_group.id
      description              = "mongodb-27017 from private SG"
    },
    {
      rule                     = "mongodb-27018-tcp"
      source_security_group_id = module.private_security_group.id
      description              = "mongodb-27018 from private SG"
    },
    {
      rule                     = "mongodb-27019-tcp"
      source_security_group_id = module.private_security_group.id
      description              = "mongodb-27019 from private SG"
    },
    {
      rule                     = "mongodb-27017-tcp"
      source_security_group_id = var.databse_computed_ingress_with_source_security_group_id
      description              = "mongodb-27017 from DB controller in private subnet"
    },
    {
      rule                     = "mongodb-27018-tcp"
      source_security_group_id = var.databse_computed_ingress_with_source_security_group_id
      description              = "mongodb-27018 from DB controller in private subnet"

    },
    {
      rule                     = "mongodb-27019-tcp"
      source_security_group_id = var.databse_computed_ingress_with_source_security_group_id
      description              = "mongodb-27019 from DB controller in private subnet"
    },
    # cross-account SG ID reference: contrary to the AWS doc, the open issue mentions no need to prepend AWS account ID
    # aws doc: https://docs.aws.amazon.com/vpc/latest/peering/vpc-peering-security-groups.html
    # github open issue: https://github.com/terraform-providers/terraform-provider-aws/issues/159
    {
      rule                     = "mongodb-27017-tcp"
      source_security_group_id = var.peer_vpc_sg_id
      description              = "mongodb-27017 from private SG in peerwell-old account"
    },
    {
      rule                     = "mongodb-27018-tcp"
      source_security_group_id = var.peer_vpc_sg_id
      description              = "mongodb-27018 from private SG in peerwell-old account"
    },
    {
      rule                     = "mongodb-27019-tcp"
      source_security_group_id = var.peer_vpc_sg_id
      description              = "mongodb-27019 from private SG in peerwell-old account"
    },
  ]

  number_of_computed_ingress_with_source_security_group_id = 9

  # Open for self (rule or from_port+to_port+protocol+description)
  ingress_with_self = [
    {
      rule = "all-all"
      description = "Self"
    },
  ]

  egress_rules = ["all-all"]
  tags         = local.db_security_group_tags
}

module "vpc_peering" {
  source = "../../resource_module/network/vpc_peering_connection"

  # passing provider to child module: https://www.terraform.io/docs/configuration/providers.html
  providers = {
    peerwell_old = "aws.peerwell_old"
  }

  ## VPC peering connection ##
  vpc_id           = module.vpc.vpc_id
  peer_owner_id    = var.peer_owner_id
  peer_vpc_id      = var.peer_vpc_id
  peer_region      = var.peer_region
  vpc_peering_tags = local.vpc_peering_tags
  accepter_tags    = local.vpc_peering_tags

  ## route ##
  requester_route_tables = var.requester_route_tables
  accepter_cidr_block    = var.accepter_cidr_block
  accepter_route_tables  = var.accepter_route_tables
  requester_cidr_block   = var.requester_cidr_block

  region = var.region
}