## AWS Key Pair ##
output "key_name" {
  description = "The key pair name."
  value       = aws_key_pair.this.key_name
}

output "fingerprint" {
  description = "The MD5 public key fingerprint as specified in section 4 of RFC 4716."
  value       = aws_key_pair.this.fingerprint
}

## TLS Private Key ##
output "algorithm" {
  description = "The algorithm that was selected for the key."
  value       = tls_private_key.this.algorithm
}
output "private_key_pem" {
  description = "The private key data in PEM format."
  value       = tls_private_key.this.private_key_pem
}
output "public_key_pem" {
  description = "The public key data in PEM format."
  value       = tls_private_key.this.public_key_pem
}
output "public_key_openssh" {
  description = "The public key data in OpenSSH authorized_keys format, if the selected private key format is compatible."
  value       = tls_private_key.this.public_key_openssh
}
output "public_key_fingerprint_md5" {
  description = "The md5 hash of the public key data in OpenSSH MD5 hash format, e.g. aa:bb:cc:.... Only available if the selected private key format is compatible, as per the rules for"
  value       = tls_private_key.this.public_key_fingerprint_md5
}