locals {
  is_t_instance_type                = replace(var.instance_type, "/^t[23]{1}\\..*$/", "1") == "1" ? true : false
  this_id                           = compact(coalescelist(aws_instance.this.*.id, [""]))
  this_availability_zone            = compact(coalescelist(aws_instance.this.*.availability_zone, [""]))
  this_key_name                     = compact(coalescelist(aws_instance.this.*.key_name, [""]))
  this_public_dns                   = compact(coalescelist(aws_instance.this.*.public_dns, [""]))
  this_public_ip                    = compact(coalescelist(aws_instance.this.*.public_ip, [""]))
  this_primary_network_interface_id = compact(coalescelist(aws_instance.this.*.primary_network_interface_id, [""]))
  this_private_dns                  = compact(coalescelist(aws_instance.this.*.private_dns, [""]))
  this_private_ip                   = compact(coalescelist(aws_instance.this.*.private_ip, [""]))
  this_placement_group              = compact(coalescelist(aws_instance.this.*.placement_group, [""]))
  this_security_groups              = coalescelist(aws_instance.this.*.security_groups, [""])
  this_vpc_security_group_ids       = coalescelist(flatten(aws_instance.this.*.vpc_security_group_ids), [""])
  this_subnet_id                    = compact(coalescelist(aws_instance.this.*.subnet_id, [""]))
  this_credit_specification         = flatten(aws_instance.this.*.credit_specification)
  this_tags                         = coalescelist(aws_instance.this.*.tags, [""])
  this_volume_tags                  = coalescelist(aws_instance.this.*.volume_tags, [""])
  this_password_data                = coalescelist(aws_instance.this.*.password_data, [""])
}