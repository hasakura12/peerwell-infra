resource "aws_ssm_parameter" "this" {
  name      = lookup(var.ssm_parameter, "name")
  type      = lookup(var.ssm_parameter, "type")
  value     = lookup(var.ssm_parameter, "value")
  overwrite = true
}