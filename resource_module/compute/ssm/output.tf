output "arn" {
  description = "The ARN of the parameter."
  value       = aws_ssm_parameter.this.arn
}
output "name" {
  description = "(Required) The name of the parameter."
  value       = aws_ssm_parameter.this.name
}
output "description" {
  description = "(Required) The description of the parameter."
  value       = aws_ssm_parameter.this.description
}
output "type" {
  description = "(Required) The type of the parameter. Valid types are String, StringList and SecureString."
  value       = aws_ssm_parameter.this.type
}
output "value" {
  description = "(Required) The value of the parameter."
  value       = aws_ssm_parameter.this.value
}
output "version" {
  description = "The version of the parameter."
  value       = aws_ssm_parameter.this.version
}