## DocDB Cluster ##
output "cluster_arn" {
  description = "Amazon Resource Name (ARN) of cluster"
  value       = aws_docdb_cluster.this.arn
}
output "cluster_members" {
  description = "List of DocDB Instances that are a part of this cluster"
  value       = aws_docdb_cluster.this.cluster_members
}
output "cluster_resource_id" {
  description = "The DocDB Cluster Resource ID"
  value       = aws_docdb_cluster.this.cluster_resource_id
}
output "cluster_endpoint" {
  description = "The DNS address of the DocDB instance"
  value       = aws_docdb_cluster.this.endpoint
}
output "hosted_zone_id" {
  description = "The Route53 Hosted Zone ID of the endpoint"
  value       = aws_docdb_cluster.this.hosted_zone_id
}
output "cluster_id" {
  description = "The DocDB Cluster Identifier"
  value       = aws_docdb_cluster.this.id
}
# output "maintenance_window" {
#   description = "The instance maintenance window"
#   value = aws_docdb_cluster.this.maintenance_window
# }
output "reader_endpoint" {
  description = "A read-only endpoint for the DocDB cluster, automatically load-balanced across replicas"
  value       = aws_docdb_cluster.this.reader_endpoint
}

## DocDB Instance ##
output "instance_arn" {
  description = "Amazon Resource Name (ARN) of cluster instance"
  value       = aws_docdb_cluster_instance.this.*.arn
}
output "db_subnet_group_name" {
  description = "The DB subnet group to associate with this DB instance."
  value       = aws_docdb_cluster_instance.this.*.db_subnet_group_name
}
output "dbi_resource_id" {
  description = "The region-unique, immutable identifier for the DB instance."
  value       = aws_docdb_cluster_instance.this.*.dbi_resource_id
}
output "instance_endpoint" {
  description = "The DNS address for this instance. May not be writable"
  value       = aws_docdb_cluster_instance.this.*.endpoint
}
output "engine_version" {
  description = "The database engine version"
  value       = aws_docdb_cluster_instance.this.*.engine_version
}
output "kms_key_id" {
  description = "The ARN for the KMS encryption key if one is set to the cluster."
  value       = aws_docdb_cluster_instance.this.*.kms_key_id
}
output "port" {
  description = "The database port"
  value       = aws_docdb_cluster_instance.this.*.port
}
output "preferred_backup_window" {
  description = "The daily time range during which automated backups are created if automated backups are enabled."
  value       = aws_docdb_cluster_instance.this.*.preferred_backup_window
}
output "storage_encrypted" {
  description = "Specifies whether the DB cluster is encrypted."
  value       = aws_docdb_cluster_instance.this.*.storage_encrypted
}
output "writer" {
  description = "Boolean indicating if this instance is writable. False indicates this instance is a read replica."
  value       = aws_docdb_cluster_instance.this.*.writer
}

## DocDB subnet group ##
output "subnet_group_id" {
  description = "The docDB subnet group name."
  value       = aws_docdb_subnet_group.this.id
}
output "subnet_group_arn" {
  description = "The ARN of the docDB subnet group."
  value       = aws_docdb_subnet_group.this.arn
}

## DocDB parameter group ##
output "parameter_group_id" {
  description = "The documentDB cluster parameter group name."
  value       = aws_docdb_cluster_parameter_group.this.id
}
output "parameter_group_arn" {
  description = "The ARN of the documentDB cluster parameter group."
  value       = aws_docdb_cluster_parameter_group.this.arn
}