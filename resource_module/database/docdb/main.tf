resource "aws_docdb_cluster" "this" {
  apply_immediately               = var.apply_immediately
  availability_zones              = var.availability_zones
  backup_retention_period         = var.backup_retention_period
  cluster_identifier              = var.cluster_identifier
  db_subnet_group_name            = aws_docdb_subnet_group.this.name
  db_cluster_parameter_group_name = aws_docdb_cluster_parameter_group.this.name
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  engine_version                  = var.engine_version
  engine                          = var.engine
  final_snapshot_identifier       = var.final_snapshot_identifier
  kms_key_id                      = var.kms_key_id
  master_password                 = var.master_password
  master_username                 = var.master_username
  port                            = var.port
  preferred_backup_window         = var.preferred_backup_window
  skip_final_snapshot             = var.skip_final_snapshot
  # snapshot_identifier             = var.snapshot_identifier
  storage_encrypted      = var.storage_encrypted
  tags                   = var.tags
  vpc_security_group_ids = var.vpc_security_group_ids

  # lifecycle {
  #   prevent_destroy = true
  # }
}

resource "aws_docdb_cluster_instance" "this" {
  count                        = var.docdb_instance_count
  apply_immediately            = var.apply_immediately
  auto_minor_version_upgrade   = var.auto_minor_version_upgrade
  availability_zone            = var.availability_zones[count.index]
  cluster_identifier           = aws_docdb_cluster.this.id
  engine                       = var.engine
  identifier                   = "${var.identifier}-${var.availability_zones[count.index]}"
  instance_class               = var.instance_class
  preferred_maintenance_window = var.preferred_maintenance_window
  tags                         = var.docdb_instance_tags
  promotion_tier               = var.promotion_tier
}

resource "aws_docdb_subnet_group" "this" {
  name       = var.subnet_group_name
  subnet_ids = var.subnet_ids
  tags       = var.subnet_group_tags
}

resource "aws_docdb_cluster_parameter_group" "this" {
  name   = var.parameter_group_name
  family = var.family

  dynamic "parameter" {
    for_each = var.parameter
    content {
      name  = parameter.value.name
      value = lookup(parameter.value, "value", null)
    }
  }

  tags = var.parameter_group_tags
}