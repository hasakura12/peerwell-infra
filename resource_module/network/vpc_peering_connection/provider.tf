# passing provider to grandchildren module: https://www.terraform.io/docs/configuration/providers.html
provider "aws" {
  region = var.region
  alias  = "peerwell_old"
}
