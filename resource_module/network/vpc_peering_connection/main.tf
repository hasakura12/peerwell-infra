########################################
# AWS VPC Peering Connection resource module
########################################

resource "aws_vpc_peering_connection" "this" {
  vpc_id        = var.vpc_id
  peer_owner_id = var.peer_owner_id
  peer_vpc_id   = var.peer_vpc_id
  peer_region   = var.peer_region
  tags          = var.vpc_peering_tags

  # won't be configured until peering request is accepted, hence initial state will
  # always be set to false, hence next "terraform apply" will find a diff and try to recreate new resource
  # requester {
  #   allow_remote_vpc_dns_resolution = true
  # }
}

resource "aws_vpc_peering_connection_accepter" "this" {
  provider = "aws.peerwell_old"

  vpc_peering_connection_id = aws_vpc_peering_connection.this.id
  auto_accept               = var.accepter_auto_accept
  tags                      = var.accepter_tags

  # accepter {
  #   allow_remote_vpc_dns_resolution = true
  # }
}

resource "aws_route" "requester_route" {
  count = length(var.requester_route_tables)

  route_table_id            = var.requester_route_tables[count.index]
  vpc_peering_connection_id = aws_vpc_peering_connection.this.id
  destination_cidr_block    = var.accepter_cidr_block
}

# resource "aws_route" "accepter_route" {
#   count = length(var.accepter_route_tables)

#   provider = "aws.peerwell_old"

#   route_table_id =  var.accepter_route_tables[count.index]
#   vpc_peering_connection_id = aws_vpc_peering_connection_accepter.this.id
#   destination_cidr_block    = var.requester_cidr_block
# }
