########################################
# Variables
########################################
# Peering
variable "vpc_id" {
  type = "string"
}
variable "peer_owner_id" {}
variable "peer_vpc_id" {}
variable "peer_region" {}
variable "vpc_peering_tags" {
  type = "map"
}

variable "requester_route_tables" {
  type = "list"
}

variable "requester_routes" {
  type    = "string"
  default = ""
}

variable "requester_cidr_block" {
  type    = "string"
  default = ""
}

variable "accepter_route_tables" {
  type    = "list"
  default = []
}

variable "accepter_auto_accept" {
  type    = "string"
  default = true
}

variable "accepter_cidr_block" {
  type    = "string"
  default = ""
}

variable "accepter_tags" {
  type    = "map"
  default = {}
}

variable "region" {
}