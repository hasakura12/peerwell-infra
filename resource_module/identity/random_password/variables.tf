variable "length" {
    description = "(Required) The length of the string desired"
}
variable "special" {
    description = "(Optional) (default true) Include special characters in random string."
}
variable "override_special" {
    description = "(Optional) Supply your own list of special characters to use for string generation. This overrides the default character list in the special argument. The special argument must still be set to true for any overwritten characters to be used in generation."
}