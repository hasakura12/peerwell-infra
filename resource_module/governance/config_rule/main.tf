########################################
# AWS Config rule resource module
#
# https://www.terraform.io/docs/providers/aws/r/config_config_rule.html
########################################

resource "aws_config_config_rule" "periodic_rule" {
  count                       = "${length(var.periodic_rules)}"
  name                        = "${lookup(var.periodic_rules[count.index], "name")}${local.suffix}"
  description                 = "${lookup(var.periodic_rules[count.index], "description")}"
  maximum_execution_frequency = "${lookup(var.periodic_rules[count.index], "maximum_execution_frequency")}"
  input_parameters            = "${var.periodic_rules_input_parameters[lookup(var.periodic_rules[count.index], "name")]}"

  source {
    owner             = "${lookup(var.periodic_rules[count.index], "owner")}"
    source_identifier = "${lookup(var.periodic_rules[count.index], "source_identifier")}"
  }
}

resource "aws_config_config_rule" "status_change_rules" {
  count            = "${length(var.status_change_rules)}"
  name             = "${lookup(var.status_change_rules[count.index], "name")}${local.suffix}"
  description      = "${lookup(var.status_change_rules[count.index], "description")}"
  input_parameters = "${var.status_change_rules_input_parameters[lookup(var.status_change_rules[count.index], "name")]}"

  source {
    owner             = "${lookup(var.status_change_rules[count.index], "owner")}"
    source_identifier = "${lookup(var.status_change_rules[count.index], "source_identifier")}"
  }
}
