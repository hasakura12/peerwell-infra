########################################
# Variables
########################################

variable "periodic_rules" {
  description = "The list of Config rules. Must contains field name, description, input_parameter, maximum_execution_frequency, owner and source_identifier."
  default     = [{}]
}

variable "periodic_rules_input_parameters" {
  description = "The list of input parameters for the periodic Config rules."
  type        = "map"
}

variable "status_change_rules" {
  description = "The list of Config rules. Must contains field name, description, input_parameter, maximum_execution_frequency, owner and source_identifier."
  default     = [{}]
}

variable "status_change_rules_input_parameters" {
  description = "The list of input parameters for the status change Config rules."
  type        = "map"
}

variable "recorder_id" {
  default     = ""
  description = "Only used for creating a terraform dependency on the config recorder modules"
}
