########################################
# Outputs
########################################

output "arn" {
  description = "The list of ARN of the config rules"
  value       = "${concat(aws_config_config_rule.periodic_rule.*.arn, aws_config_config_rule.status_change_rules.*.arn)}"
}

output "id" {
  description = "The list of ID of the config rules"
  value       = "${concat(aws_config_config_rule.periodic_rule.*.id, aws_config_config_rule.status_change_rules.*.id)}"
}

output "status_change_arn" {
  description = "The list of ARN of the status change config rules"
  value       = "${concat(aws_config_config_rule.periodic_rule.*.arn, aws_config_config_rule.status_change_rules.*.arn)}"
}

output "status_change_id" {
  description = "The list of ID of the  status change config rules"
  value       = "${concat(aws_config_config_rule.periodic_rule.*.id, aws_config_config_rule.status_change_rules.*.id)}"
}
