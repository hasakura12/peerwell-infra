resource "aws_organizations_policy" "this" {
  description = "${var.description}"
  content     = "${var.content}"
  name        = "${var.name}"
  type        = "${var.type}"
}

resource "aws_organizations_policy_attachment" "this" {
  policy_id = "${aws_organizations_policy.this.id}"
  target_id = "${var.target_id}"
}
