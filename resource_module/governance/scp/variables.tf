variable "content" {
  type        = "string"
  description = "The policy content to add to the new policy."
}

variable "description" {
  type        = "string"
  description = "A description to assign to the policy."
}

variable "name" {
  type        = "string"
  description = "The friendly name to assign to the policy."
}

variable "target_id" {
  type        = "string"
  description = "The unique identifier (ID) of the root, organizational unit, or account number that you want to attach the policy to."
}

variable "type" {
  type        = "string"
  description = "A description to assign to the policy."
}
