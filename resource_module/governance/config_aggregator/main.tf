########################################
# AWS Config Aggregator resource module
#
# https://www.terraform.io/docs/providers/aws/r/config_configuration_aggregator.html
########################################

resource "aws_config_configuration_aggregator" "this" {
  name = "${var.name}"

  organization_aggregation_source {
    all_regions = "${var.all_regions}"
    role_arn    = "${var.aggregator_role_arn}"
  }
}
