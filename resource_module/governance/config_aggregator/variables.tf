########################################
# Variables
########################################

variable "name" {
  description = "The name of the configuration aggregator."
  type        = "string"
}

variable "all_regions" {
  description = "If true, aggregate existing AWS Config regions and future regions."
  type        = "string"
}

variable "aggregator_role_arn" {
  description = "ARN of the IAM role used to retrieve AWS Organization details associated with the aggregator account."
  type        = "string"
}
