########################################
# Outputs
########################################

output "arn" {
  description = "The ARN of the aggregator"
  value       = "${aws_config_configuration_aggregator.this.arn}"
}
