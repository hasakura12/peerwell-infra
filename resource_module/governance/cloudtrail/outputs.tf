########################################
# Outputs
########################################

output "arn" {
  description = "The Amazon Resource Name of the trail."
  value       = "${aws_cloudtrail.this.arn}"
}

output "id" {
  description = "The name of the trail."
  value       = "${aws_cloudtrail.this.id}"
}

output "home_region" {
  description = "The region in which the trail was created."
  value       = "${aws_cloudtrail.this.home_region}"
}
