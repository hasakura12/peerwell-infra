########################################
# Variables
########################################

variable "name" {
  description = "The name of the recorder. Changing it recreates the resource."
  type        = "string"
}

variable "config_role_arn" {
  description = "Amazon Resource Name (ARN) of the IAM role. used to make read or write requests to the delivery channel and to describe the AWS resources associated with the account."
  type        = "string"
}

variable "all_supported" {
  description = "Specifies whether AWS Config records configuration changes for every supported type of regional resource (which includes any new type that will become supported in the future)."
  type        = "string"
}

variable "include_global_resource_types" {
  description = "Specifies whether AWS Config includes all supported types of global resources with the resources that it records. Requires all_supported = true."
  type        = "string"
}

variable "delivery_channel_name" {
  description = "The name of the delivery channel."
  type        = "string"
}

variable "s3_bucket_name" {
  description = "The name of the S3 bucket used to store the configuration history."
  type        = "string"
}

variable "s3_key_prefix" {
  description = "The prefix for the specified S3 bucket."
  type        = "string"
}

variable "delivery_frequency" {
  description = "Options for how AWS Config delivers configuration snapshots."
  type        = "string"
}

variable "is_enabled" {
  description = "Whether the configuration recorder should be enabled or disabled."
  type        = "string"
}
