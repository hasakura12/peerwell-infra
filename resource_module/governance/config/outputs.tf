########################################
# Outputs
########################################

output "id" {
  description = "Name of the recorder"
  value       = "${aws_config_configuration_recorder.this.id}"
}

output "recorder_is_enabled" {
  description = "Whether the configuration recorder should be enabled or disabled."
  value       = "${aws_config_configuration_recorder_status.this.is_enabled}"
}
