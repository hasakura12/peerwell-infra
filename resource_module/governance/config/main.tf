########################################
# AWS Config resource module
#
# https://www.terraform.io/docs/providers/aws/r/config_configuration_recorder.html
# https://www.terraform.io/docs/providers/aws/r/config_configuration_recorder_status.html
# https://www.terraform.io/docs/providers/aws/r/config_config_rule.html
########################################

resource "aws_config_configuration_recorder" "this" {
  name     = "${var.name}"
  role_arn = "${var.config_role_arn}"

  recording_group = {
    all_supported                 = "${var.all_supported}"
    include_global_resource_types = "${var.include_global_resource_types}"
  }
}

resource "aws_config_delivery_channel" "this" {
  name           = "${var.delivery_channel_name}"
  s3_bucket_name = "${var.s3_bucket_name}"
  s3_key_prefix  = "${var.s3_key_prefix}"

  snapshot_delivery_properties = {
    delivery_frequency = "${var.delivery_frequency}"
  }

  depends_on = ["aws_config_configuration_recorder.this"]
}

resource "aws_config_configuration_recorder_status" "this" {
  name       = "${aws_config_configuration_recorder.this.id}"
  is_enabled = "${var.is_enabled}"
  depends_on = ["aws_config_delivery_channel.this"]
}
