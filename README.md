# Get Started With AWS Infra with Terraform

Index:

* Architecture Diagram
* Cost Consideration
* MongoDB Connectivity (migrated to AWS DocumentDB)
    * a. From Mongo shell
        * How to ssh forward into DB Controller instance from your local laptop
        * Get ssh key from AWS Parameter Store
        * SSH-forward into the bastion
        * SSH-forward into the DB controller from the bastion
        * Get mongoshell from the DB controller
    * b. From Mongo Node.js
    * c. From Mongo Node.js in local dev environment through "localhost:27017" (multi-hop ssh tunnel to DocDB cluster)
* MongoDB Operation
    * Restore Mongo backup to AWS DocDB
* Future Containerlization Plan
    * CNCF Trail Map

## Architecture Diagram (as of Oct 2019)
> some components are not in place yet (e.g. APIs in private subnet in peerwell-devops account, CloudWatch Log, etc)

> this is a current architecture and not a representation of future architecture which would entail containerlization using Kubernetes and CI/CD pipeline etc

- VPC peering, networking, DocDB cluster, Bastions, DB-controllers are deployed
- DocDB cluster allows access only from AWS intra-/inter-account VPC networks or AWS services.
- VPC peering enables connection from APIs in private subnet in `peerwell-old` account to DocDB cluster in databse subnet in `peerwell-devops` account, along with related security group rules and route tables
- "DNS resolution from accepter VPC to private IP" in VPC peering config is not necessary
- Route table entry for destination CIDR and VPC peering ID as target is necessary

![alt text](imgs/architecture_diagram.png "Infra Architecture Diagram")

## Cost Consideration
Instance types of Bastion, DB Controller, and DocDB Cluster instances are defined in [terraform.tfvars](composition/api-infra/us-east-1/peerwell-devops/terraform.tfvars):
```
bastion_instance_type  = "t2.micro"
db_controller_instance_type  = "t2.micro"
docdb_instance_class          = "db.r4.large"
```

## Mongo DB Connectivity
### a. From Mongo shell
#### How to ssh forward into DB Controller instance from your local laptop
local laptop -> Bastion in public subnet -> DB Controller in private subnet where it can access DocDB in database subnet

#### Get ssh key from AWS Parameter Store
> Note: before proceeding with the below instructions, your IP needs be whitelisted in the bastion security group. To do so, you should give your IP to the DevOp engineer.

<details><summary>show</summary>
<p>

Copy output manually (assigning the output to variable doesn't work)
```
aws ssm get-parameter --name /bastion/keypair/private_key --region us-east-1 --with-decryption --query "Parameter.Value" --output text | sed '$d'
```

Paste the output into a file
```
mkdir ~/.ssh/peerwell-devops
vim ~/.ssh/peerwell-devops/bastion.key
chmod 600 ~/.ssh/peerwell-devops/bastion.key
ssh-add -D 
ssh-add -K ~/.ssh/peerwell-devops/bastion.key
```
</p>
</details>

#### SSH-forward into the bastion
<details><summary>show</summary>
<p>

```
{
  bastion_endpoint=$(aws ssm get-parameter --name /bastion/endpoint --region us-east-1 --query "Parameter.Value" --output text)

  ssh -A ec2-user@${bastion_endpoint}
}
```

</p>
</details>

#### SSH-forward into the DB controller from the bastion
<details><summary>show</summary>
<p>

```
{
  db_controller_endpoint=$(aws ssm get-parameter --name /db_controller/endpoint --region us-east-1 --query "Parameter.Value" --output text)

  ssh ${db_controller_endpoint}
}
```

</p>
</details>

##### You could combine the above two ssh commands into one:
<details><summary>show</summary>
<p>

```
bastion_endpoint=$(aws ssm get-parameter --name /bastion/endpoint --region us-east-1 --query "Parameter.Value" --output text)

db_controller_endpoint=$(aws ssm get-parameter --name /db_controller/endpoint --region us-east-1 --query "Parameter.Value" --output text)

ssh -A -t \
  ec2-user@${bastion_endpoint} \
  ssh -A -t \
  ec2-user@${db_controller_endpoint}
```

</p>
</details>

#### Get mongoshell from the DB controller
<details><summary>show</summary>
<p>

```
{
  host=$(aws ssm get-parameter --name /docdb/cluster_endpoint --region us-east-1 --query "Parameter.Value" --output text)
  
  master_username=$(aws ssm get-parameter --name /docdb/master_username --region us-east-1 --query "Parameter.Value" --output text)
  
  master_password=$(aws ssm get-parameter --name /docdb/master_password --region us-east-1 --with-decryption --query "Parameter.Value" --output text)

  wget https://s3.amazonaws.com/rds-downloads/rds-combined-ca-bundle.pem

  mongo --host ${host}:27017 \
    --username ${master_username} \
    --password ${master_password}
}
```

</p>
</details>

### b. From Mongo Node.js
Refer to the [AWS doc](https://docs.aws.amazon.com/documentdb/latest/developerguide/connect.html), but it'd look something like below:
```
var MongoClient = require('mongodb').MongoClient;
                  
//Create a MongoDB client, open a connection to Amazon DocumentDB as a replica set,
//  and specify the read preference as secondary preferred
var client = MongoClient.connect(
'mongodb://<dbusername>:<dbpassword>@mycluster.node.us-east-1.docdb.amazonaws.com:27017/test?replicaSet=rs0&readPreference=secondaryPreferred', 
{
  useNewUrlParser: true
},

function(err, client) {
    if(err)
        throw err;
    //Specify the database to be used
    db = client.db('test');
    
    //Specify the collection to be used
    col = db.collection('col');

    //Insert a single document
    col.insertOne({'hello':'Amazon DocumentDB'}, function(err, result){
      //Find the document that was previously written
      col.findOne({'hello':'Amazon DocumentDB'}, function(err, result){
        //Print the result to the screen
        console.log(result);
        
        //Close the connection
        client.close()
      });
   });
});
```

### c. From Mongo Node.js in local dev environment through "localhost:27017" (multi-hop ssh tunnel to DocDB cluster)
With ssh tunneling, AWS DocDB can be accessed from `localhost:27017` or `mongo` on local machine. This is useful when you are developing APIs on a _local machine_ and want to test APIs against MongoDB on AWS DocDB.

Get local mongoshell by establishing ssh tunnels from local -> bastion -> DB controller -> DocDB cluster
<details><summary>show</summary>
<p>

```
{
  bastion_endpoint=$(aws ssm get-parameter --name /bastion/endpoint --region us-east-1 --query "Parameter.Value" --output text)

  db_controller_endpoint=$(aws ssm get-parameter --name /db_controller/endpoint --region us-east-1 --query "Parameter.Value" --output text)

  host=$(aws ssm get-parameter --name /docdb/cluster_endpoint --region us-east-1 --query "Parameter.Value" --output text)
}

## 2-hop ssh tunneling: local -> bastion -> DB controller -> DocDB cluster

# don't add -N option to 1st ssh command because we will chain 2nd ssh command from 1st hop
# add -f option to create ssh tunnel in a background process detached from a terminal, so the tunnel persists after closing terminal

ssh -A -f -t \
  ec2-user@${bastion_endpoint} \
  -L 27017:localhost:27017 \
  ssh -A -N -t \
  ec2-user@${db_controller_endpoint} \
  -L 27017:${host}:27017
```

From another terminal in your laptop, execute the mongo command:

> Note: mongo shouldn't be running locally on port 27017,  otherwise ssh tunnel can't use the same port. Run `lsof -nai4 | grep LISTEN` to check which ports are being used.

```
# access mongo shell from outside VPC (note: no specifying host as it's localhost)

{
  master_username=$(aws ssm get-parameter --name /docdb/master_username --region us-east-1 --query "Parameter.Value" --output text)
    
  master_password=$(aws ssm get-parameter --name /docdb/master_password --region us-east-1 --with-decryption --query "Parameter.Value" --output text)

  mongo --host localhost:27017 \
    --username ${master_username} \
    --password ${master_password} \
    --verbose
}
```
You should get mongoshell:
```
connecting to: mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb
2019-10-23T20:31:07.187+0200 D1 NETWORK  [js] creating new connection to:127.0.0.1:27017
2019-10-23T20:31:07.571+0200 D1 -        [js] User Assertion: UnsupportedFormat: Invalid IP address in CIDR string src/mongo/util/net/cidr.cpp 116
2019-10-23T20:31:07.571+0200 D1 -        [js] User Assertion: UnsupportedFormat: Invalid IP address in CIDR string src/mongo/util/net/cidr.cpp 116
2019-10-23T20:31:07.571+0200 D1 -        [js] User Assertion: UnsupportedFormat: Invalid IP address in CIDR string src/mongo/util/net/cidr.cpp 116
2019-10-23T20:31:07.571+0200 D1 -        [js] User Assertion: UnsupportedFormat: Invalid IP address in CIDR string src/mongo/util/net/cidr.cpp 116
2019-10-23T20:31:07.571+0200 D1 -        [js] User Assertion: UnsupportedFormat: Invalid IP address in CIDR string src/mongo/util/net/cidr.cpp 116
2019-10-23T20:31:07.571+0200 D1 -        [js] User Assertion: UnsupportedFormat: Invalid IP address in CIDR string src/mongo/util/net/cidr.cpp 116
2019-10-23T20:31:07.571+0200 D1 -        [js] User Assertion: UnsupportedFormat: Invalid IP address in CIDR string src/mongo/util/net/cidr.cpp 116
2019-10-23T20:31:07.571+0200 W  NETWORK  [js] The server certificate does not match the host name. Hostname: 127.0.0.1 does not match SAN(s): docdb-cluster-ue1-prod-peerwell-api-infra.cluster-ci5vx8214orr.us-east-1.docdb.amazonaws.com docdb-cluster-ue1-prod-peerwell-api-infra.cluster-ro-ci5vx8214orr.us-east-1.docdb.amazonaws.com docdb-instance-ue1-prod-peerwell-api-infra-us-east-1a.ci5vx8214orr.us-east-1.docdb.amazonaws.com 
2019-10-23T20:31:07.572+0200 D1 NETWORK  [js] connected to server 127.0.0.1:27017
2019-10-23T20:31:07.695+0200 D1 NETWORK  [js] connected connection!
Implicit session: session { "id" : UUID("0b6e86f8-e5ae-4920-9eb1-3cc6dc36ac34") }
MongoDB server version: 3.6.0
WARNING: shell and server versions do not match

Warning: Non-Genuine MongoDB Detected

This server or service appears to be an emulation of MongoDB rather than an official MongoDB product.

Some documented MongoDB features may work differently, be entirely missing or incomplete, or have unexpected performance characteristics.

To learn more please visit: https://dochub.mongodb.org/core/non-genuine-mongodb-server-warning.

rs0:PRIMARY>
```
We just verified a connection from a local machine to AWS DocDB cluster is established. As long as a ssh tunneling command is executed, the Node.js code should also be able to connect to DocDB cluster endpoint using `localhost:27017`.

</p>
</details>

## Mongo Operation
### Restore MongoDB from backup

<details><summary>show</summary>
<p>

```
## Pre-requisites:
# 1. Use AMI name "ec2-ue1-prod-db_controller" that has mongo tools pre-installed to launch DB controller
# 2. DB controller's storage must be large enough (20+GB) to be able to uncompress the DB snapshot
# 3. Use large instance type (i.e. t3.xlarge) to get faster throughput to expedite mongorestore backup
# 4. On DB controller in peerwell-devops account, manually configure "aws configure" with credentials for peerwell-old account because S3 backup belongs to their account

# fetch Mongo snapshot from S3 bucket
aws s3 cp s3://peerwell-db-backups/mongodb-2019-10-25-06-30-01.dump.tar.gz .

# untar and uncompress it
mkdir mongodb-2019-10-25-06-30-01
tar -xvzf mongodb-2019-10-25-06-30-01.dump.tar.gz -C mongodb-2019-10-25-06-30-01
```

After uncompressing, disk usage is as following (about 9.8GB used):
```
[ec2-user@ip-10-1-101-253 ~]$ du -h
4.0K    ./.ssh
8.0K    ./.aws
0       ./apputils/BACKUPS/mongodb/mongodb-2019-10-25-06-30-01.dump/PeerWell_Production
4.0K    ./apputils/BACKUPS/mongodb/mongodb-2019-10-25-06-30-01.dump/PeerWell-Development
0       ./apputils/BACKUPS/mongodb/mongodb-2019-10-25-06-30-01.dump/PeerWell-Test
0       ./apputils/BACKUPS/mongodb/mongodb-2019-10-25-06-30-01.dump/test
0       ./apputils/BACKUPS/mongodb/mongodb-2019-10-25-06-30-01.dump/wellness_development
2.8M    ./apputils/BACKUPS/mongodb/mongodb-2019-10-25-06-30-01.dump/PeerWell-Staging
20K     ./apputils/BACKUPS/mongodb/mongodb-2019-10-25-06-30-01.dump/admin
7.8G    ./apputils/BACKUPS/mongodb/mongodb-2019-10-25-06-30-01.dump/PeerWell-Production
0       ./apputils/BACKUPS/mongodb/mongodb-2019-10-25-06-30-01.dump/system
7.8G    ./apputils/BACKUPS/mongodb/mongodb-2019-10-25-06-30-01.dump
7.8G    ./apputils/BACKUPS/mongodb
7.8G    ./apputils/BACKUPS
7.8G    ./apputils
8.6G    .
[ec2-user@ip-10-1-101-253 ~]$ df -h
Filesystem      Size  Used Avail Use% Mounted on
devtmpfs        3.9G     0  3.9G   0% /dev
tmpfs           3.9G     0  3.9G   0% /dev/shm
tmpfs           3.9G  360K  3.9G   1% /run
tmpfs           3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/nvme0n1p1   20G  9.8G   11G  49% /
tmpfs           788M     0  788M   0% /run/user/1000
```
Set shell variables
```
host=docdb-2019-10-25-09-42-07.ci5vx8214orr.us-east-1.docdb.amazonaws.com
master_username=masterusername
master_password=masterpassword
backup_root_dir=mongodb-2019-10-25-06-30-01
```

Restore mongo backup to DocDB cluster
```
{
  mongorestore --host ${host}:27017 \
    --username ${master_username} \
    --password ${master_password} \
    -d PeerWell-Staging \
    ${backup_root_dir}/apputils/BACKUPS/mongodb/${backup_root_dir}.dump/PeerWell-Staging/

  mongorestore --host ${host}:27017 \
    --username ${master_username} \
    --password ${master_password} \
    -d PeerWell-Development \
    ${backup_root_dir}/apputils/BACKUPS/mongodb/${backup_root_dir}.dump/PeerWell-Development/

  2019-10-14T17:00:58.990+0000    the --db and --collection args should only be used when restoring from a BSON file. Other uses are deprecated and will not exist in the future; use --nsInclude instead
2019-10-14T17:00:58.990+0000    building a list of collections to restore from ${backup_root_dir}/apputils/BACKUPS/mongodb/${backup_root_dir}.dump/PeerWell-Development dir
2019-10-14T17:00:58.990+0000    done

  mongorestore --host ${host}:27017 \
    --username ${master_username} \
    --password ${master_password} \
    ${backup_root_dir}/apputils/BACKUPS/mongodb/${backup_root_dir}.dump/PeerWell_Production/

  mongorestore --host ${host}:27017 \
    --username ${master_username} \
    --password ${master_password} \
    -d PeerWell-Production \
    ${backup_root_dir}/apputils/BACKUPS/mongodb/${backup_root_dir}.dump/PeerWell-Production/

  mongorestore --host ${host}:27017 \
    --username ${master_username} \
    --password ${master_password} \
    ${backup_root_dir}/apputils/BACKUPS/mongodb/${backup_root_dir}.dump/system/

  mongorestore --host ${host}:27017 \
    --username ${master_username} \
    --password ${master_password} \
    ${backup_root_dir}/apputils/BACKUPS/mongodb/${backup_root_dir}.dump/wellness_development/

  mongorestore --host ${host}:27017 \
    --username ${master_username} \
    --password ${master_password} \
    ${backup_root_dir}/apputils/BACKUPS/mongodb/${backup_root_dir}.dump/test/

  mongorestore --host ${host}:27017 \
    --username ${master_username} \
    --password ${master_password} \
    ${backup_root_dir}/apputils/BACKUPS/mongodb/${backup_root_dir}.dump/admin/
}
```

Verify 
```
{
  show dbs
  use PeerWell-Staging
  db.getName()
  show collections
}
```

Connect to this cluster with an application
```
{
  host=$(aws ssm get-parameter --name /docdb/cluster_endpoint --region us-east-1 --query "Parameter.Value" --output text)
  
  master_username=$(aws ssm get-parameter --name /docdb/master_username --region us-east-1 --query "Parameter.Value" --output text)
  
  master_password=$(aws ssm get-parameter --name /docdb/master_password --region us-east-1 --with-decryption --query "Parameter.Value" --output text)
  
  mongodb://<master_username>:<master_password>@<host>:27017/?ssl=true&ssl_ca_certs=rds-combined-ca-bundle.pem&replicaSet=rs0
}
```

</p>
</details>

### SSH into DB controller in peerwell-old account
<details><summary>show</summary>
<p>

```
ssh-add -K ~/.ssh/peerwell-old/bastion.key
ssh -A hisashi@54.164.78.230
ssh -i ~/.ssh/db_controller.key ec2-user@10.0.1.5
```

</p>
</details>

## Future Containerlization Plan
### CNCF Trail Map is a good reference (https://github.com/cncf/trailmap)
![alt text](imgs/cncf_trail_map.png "CNCF Trail Map")